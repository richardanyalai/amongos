//! Macros for logging events

use core::fmt;

use crate::drivers::{
	peripheral::serial::{print_serial, println_serial},
	tty::{print, println, tty_set_fgcolor},
};

#[allow(unused)]
pub enum Color {
	Clear,
	Black,
	Red,
	Green,
	Yellow,
	Blue,
	Magenta,
	Cyan,
	White,
	Gray,
	BrightRed,
	BrightGreen,
	BrightYellow,
	BrightBlue,
	BrightMagenta,
	BrightCyan,
	BrightWhite,
}

#[rustfmt::skip]
impl fmt::Display for Color {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		use Color::*;

		write!(
			f,
			"{}",
			match self {
				Clear         => "\x1B[0m",
				Black         => "\x1B[30m",
				Red           => "\x1B[31m",
				Green         => "\x1B[32m",
				Yellow        => "\x1B[33m",
				Blue          => "\x1B[34m",
				Magenta       => "\x1B[35m",
				Cyan          => "\x1B[36m",
				White         => "\x1B[37m",
				Gray          => "\x1B[90m",
				BrightRed     => "\x1B[91m",
				BrightGreen   => "\x1B[92m",
				BrightYellow  => "\x1B[93m",
				BrightBlue    => "\x1B[94m",
				BrightMagenta => "\x1B[95m",
				BrightCyan    => "\x1B[96m",
				BrightWhite   => "\x1B[97m",
			}
		)
	}
}

pub macro log {
	($($x:tt)*) => {
		println!($($x)*);
		println_serial!($($x)*);
	}
}

pub macro logsl {
	($($x:tt)*) => {
		print!($($x)*);
		print_serial!($($x)*);
	}
}

pub macro dbg($e:expr) {{
	let value = $e;
	log!(
		"[{}:{}] {} = {:#?}",
		file!(),
		line!(),
		stringify!($e),
		value
	);
	value
}}

pub macro kok {
	($($x:tt)*) => {
		logsl!(
			"[ {}OKAY{} ] ",
			crate::utils::logger::Color::BrightGreen,
			crate::utils::logger::Color::Clear
		);
		log!($($x)*);
	}
}

pub macro kwarn {
	($($x:tt)*) => {
		logsl!(
			"[ {}WARN{} ] ",
			crate::utils::logger::Color::BrightYellow,
			crate::utils::logger::Color::Clear
		);
		log!($($x)*);
	}
}

pub macro kerr {
	($($x:tt)*) => {
		logsl!(
			"[ {}FAIL{} ] ",
			crate::utils::logger::Color::BrightRed,
			crate::utils::logger::Color::Clear
		);
		log!($($x)*);
	}
}

pub macro kinf {
	($($x:tt)*) => {
		logsl!(
			"[ {}INFO{} ] ",
			crate::utils::logger::Color::BrightCyan,
			crate::utils::logger::Color::Clear
		);
		tty_set_fgcolor!(crate::graphx::color::Colors::LightGray);
		log!($($x)*);
		tty_set_fgcolor!(crate::graphx::color::Colors::White);
	}
}
