//! Copyright (c) 2006-2007 -  http://brynet.biz.tm - <brynet@gmail.com>
//! All rights reserved.
//!
//! Redistribution and use in source and binary forms, with or without
//! modification, are permitted provided that the following conditions
//! are met:
//! 1. Redistributions of source code must retain the above copyright notice,
//!    this list of conditions and the following disclaimer.
//! 2. Redistributions in binary form must reproduce the above copyright
//!    notice, this list of conditions and the following disclaimer in the
//!    documentation and/or other materials provided with the distribution.
//! 3. The name of the author may not be used to endorse or promote products
//!    derived from this software without specific prior written permission.
//!
//! THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
//! INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
//! AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
//! THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//! EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//! PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
//! OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
//! WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
//! OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
//! ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//!
//! https://forum.osdev.org/viewtopic.php?t=11998

use alloc::{format, string::String};
use core::arch::asm;

use crate::utils::logger::kinf;

use spin::Mutex;

/// Intel-specified brand list
const INTEL: &[&str] = &[
	"Brand ID Not Supported.",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Intel(R) Pentium(R) III Xeon(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Reserved",
	"Mobile Intel(R) Pentium(R) III processor-M",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Xeon(R) Processor",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Mobile Intel(R) Pentium(R) 4 processor-M",
	"Mobile Intel(R) Pentium(R) Celeron(R) processor",
	"Reserved",
	"Mobile Genuine Intel(R) processor",
	"Intel(R) Celeron(R) M processor",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Celeron(R) processor",
	"Mobile Geniune Intel(R) processor",
	"Intel(R) Pentium(R) M processor",
	"Mobile Intel(R) Celeron(R) processor",
];

/// This table is for those brand strings that have two values
/// depending on the processor signature. It should have the
/// same number of entries as the above table.
const INTEL_OTHER: &[&str] = &[
	"Reserved",
	"Reserved",
	"Reserved",
	"Intel(R) Celeron(R) processor",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Reserved",
	"Intel(R) Xeon(R) processor",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
];

/// Struct representing a CPU
/// 
/// - `typ` - CPU type
/// - `family` - CPU family
/// - `model` - CPU model
/// - `brand` - CPU brand
#[rustfmt::skip]
#[allow(unused)]
#[derive(Default)]
struct CpuData {
	typ   : String,
	family: String,
	model : String,
	brand : String,
}

lazy_static! {
	static ref CPU_DATA: Mutex<CpuData> = Mutex::new(CpuData::default());
}

/// Wrapper around the cpuid
///
/// - `a` - eax (also serves as input)
/// - `b` - ebx
/// - `c` - ecx
/// - `d` - edx
///
/// # Safety
///
/// This function is unsafe because it calls inline assembly
#[inline]
unsafe fn cpuid(a: &mut u32, b: &mut u32, c: &mut u32, d: &mut u32) {
	#[rustfmt::skip]
	asm!(
		"mov {0:r}, rbx",
		"cpuid",
		"xchg {0:r}, rbx",
		out(reg)     *b,
		inout("eax") *a,
		out("ecx")   *c,
		out("edx")   *d
	);
}

/// Stores data about the CPU in the static object
///
/// - `dest` - reference to the destination array
/// - `data` - the data that has to be split into bytes and stored in the array
fn put_cpu_data(dest: &mut String, data: &str) {
	for byte in data.bytes() {
		dest.push(byte as char);
	}
}

/// Concatenates the value of the registers and writes it into the object
/// 
/// - `eax` - value of the eax register
/// - `ebx` - value of the eax register
/// - `ecx` - value of the eax register
/// - `edx` - value of the eax register
#[rustfmt::skip]
fn print_regs(eax: u32, ebx: u32, ecx: u32, edx: u32) {
	let mut string: [u8; 16] = [0; 16];

	for i in 0..4 {
		string[i     ] = (eax >> (8 * i)) as u8;
		string[i +  4] = (ebx >> (8 * i)) as u8;
		string[i +  8] = (ecx >> (8 * i)) as u8;
		string[i + 12] = (edx >> (8 * i)) as u8;
	}

	let brand = core::str::from_utf8(&string).unwrap();

	put_cpu_data(&mut (CPU_DATA.lock()).brand, brand);
}

/// Intel-specific checking
fn do_intel() {
	let (mut eax, mut ebx, mut ecx, mut edx) = (1u32, 0_u32, 0_u32, 0_u32);

	unsafe { cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx) }

	let model = (eax >> 4) & 0xf;
	let family = (eax >> 8) & 0xf;
	let typ = (eax >> 12) & 0x3;
	let brand = ebx & 0xff;
	let signature = eax;

	match typ {
		0 => put_cpu_data(&mut (CPU_DATA.lock()).model, "Original OEM"),
		1 => put_cpu_data(&mut (CPU_DATA.lock()).model, "Overdrive"),
		2 => put_cpu_data(&mut (CPU_DATA.lock()).model, "Dual-capable"),
		_ => put_cpu_data(&mut (CPU_DATA.lock()).model, "Reserved"),
	}

	match family {
		3 => put_cpu_data(&mut (CPU_DATA.lock()).family, "i386"),
		4 => {
			put_cpu_data(&mut (CPU_DATA.lock()).family, "i486");

			match model {
				0 | 1 => put_cpu_data(&mut (CPU_DATA.lock()).model, "DX"),
				2 => put_cpu_data(&mut (CPU_DATA.lock()).model, "SX"),
				3 => put_cpu_data(&mut (CPU_DATA.lock()).model, "487/DX2"),
				4 => put_cpu_data(&mut (CPU_DATA.lock()).model, "SL"),
				5 => put_cpu_data(&mut (CPU_DATA.lock()).model, "SX2"),
				7 => put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Write-back enhanced DX2",
				),
				8 => put_cpu_data(&mut (CPU_DATA.lock()).model, "DX4"),
				_ => {}
			}
		}
		5 => {
			put_cpu_data(&mut (CPU_DATA.lock()).family, "Pentium");

			match model {
				1 => put_cpu_data(&mut (CPU_DATA.lock()).model, "60/66"),
				2 => put_cpu_data(&mut (CPU_DATA.lock()).model, "75-200"),
				3 => put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"for 486 system",
				),
				4 => put_cpu_data(&mut (CPU_DATA.lock()).model, "MMX"),
				_ => {}
			}
		}
		6 => {
			put_cpu_data(&mut (CPU_DATA.lock()).family, "Pentium Pro");

			match model {
				1 => put_cpu_data(&mut (CPU_DATA.lock()).model, "Pentium Pro"),

				3 => put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Pentium II Model 3",
				),

				5 => put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Pentium II Model 5/Xeon/Celeron",
				),

				6 => put_cpu_data(&mut (CPU_DATA.lock()).model, "Celeron"),

				7 => put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Pentium III/Pentium III Xeon - external L2 cache",
				),

				8 => put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Pentium III/Pentium III Xeon - internal L2 cache",
				),
				_ => {}
			}
		}
		15 => put_cpu_data(&mut (CPU_DATA.lock()).family, "Pentium 4"),
		_ => {}
	}

	eax = 0x80000000;

	unsafe {
		cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx);
	}

	// Quok said: If the max extended eax value is high enough to support the
	// processor brand string (values 0x80000002 to 0x80000004), then we'll use
	// that information to return the brand information. Otherwise, we'll refer
	// back to the brand tables above for backwards compatibility with older
	// processors.
	// According to the Sept. 2006 Intel Arch Software Developer's Guide,
	// if extended eax values are supported, then all 3 values for the
	// processor brand string are supported, but we'll test just to make sure
	// and be safe.
	if eax >= 0x80000002 {
		for i in 0x80000002..=0x80000004 {
			eax = i;

			unsafe { cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx) }

			print_regs(eax, ebx, ecx, edx);
		}
	} else if brand > 0 {
		if brand < 0x18 {
			put_cpu_data(
				&mut (CPU_DATA.lock()).brand,
				if signature == 0x000006B1 || signature == 0x00000F13 {
					INTEL_OTHER
				} else {
					INTEL
				}[brand as usize],
			);
		} else {
			put_cpu_data(&mut (CPU_DATA.lock()).brand, "Reserved");
		}
	}
}

/// AMD-specific checking
fn do_amd() {
	let (mut eax, mut ebx, mut ecx, mut edx) = (1u32, 0_u32, 0_u32, 0_u32);

	unsafe { cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx) }

	let model = ((eax >> 4) & 0xF) as i32;
	let family = ((eax >> 8) & 0xF) as i32;

	match family {
		4 => {
			put_cpu_data(&mut (CPU_DATA.lock()).model, "486 Model 4");
		}
		5 => match model {
			0..=7 => {
				put_cpu_data(&mut (CPU_DATA.lock()).model, "K6 Model 7");
			}
			8 => {
				put_cpu_data(&mut (CPU_DATA.lock()).model, "K6-2 Model 8");
			}
			9 => {
				put_cpu_data(&mut (CPU_DATA.lock()).model, "K6-III Model 9");
			}
			_ => {
				put_cpu_data(&mut (CPU_DATA.lock()).model, "K5/K6 Model ");
				put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					&format!("{}", model)[..],
				);
			}
		},
		6 => match model {
			1..=3 => {
				put_cpu_data(&mut (CPU_DATA.lock()).model, "Duron Model 3");
			}
			4 => {
				put_cpu_data(&mut (CPU_DATA.lock()).model, "Athlon Model 4");
			}
			6 => {
				put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Athlon MP/Mobile Athlon Model 6",
				);
			}
			7 => {
				put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Mobile Duron Model 7",
				);
			}
			_ => {
				put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					"Duron/Athlon Model ",
				);
				put_cpu_data(
					&mut (CPU_DATA.lock()).model,
					&format!("{}", model)[..],
				);
			}
		},
		_ => {
			put_cpu_data(
				&mut (CPU_DATA.lock()).family,
				&format!("{}", family)[..],
			);
			put_cpu_data(
				&mut (CPU_DATA.lock()).model,
				&format!("{}", model)[..],
			);
		}
	}

	eax = 0x80000000;

	unsafe { cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx) }

	if eax == 0 {
		return;
	}

	if eax >= 0x80000002 {
		for i in 0x80000002..=0x80000004 {
			eax = i;

			unsafe { cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx) }

			print_regs(eax, ebx, ecx, edx);
		}
	}
}

/// Detects which CPU is present and collects information about the CPU
pub fn detect_cpu() {
	let (mut eax, mut ebx, mut ecx, mut edx) = (0_u32, 0_u32, 0_u32, 0_u32);

	unsafe {
		cpuid(&mut eax, &mut ebx, &mut ecx, &mut edx);
	}

	#[rustfmt::skip]
	match ebx {
		0x756e6547 => do_intel(),
		0x68747541 => do_amd(),
		_          => panic!("Invalid CPU magic code"),
	}

	kinf!("{}", CPU_DATA.lock().brand);
}
