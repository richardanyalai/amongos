//! Panic

/// Panic handler method
#[cfg(not(test))]
#[panic_handler]
pub fn panic(info: &core::panic::PanicInfo) -> ! {
	use crate::{interrupts, log, HALT};

	log!("{info}");

	unsafe {
		interrupts::disable();

		loop {
			HALT!();
		}
	}
}
