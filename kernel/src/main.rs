//! Initialization of the drivers and other OS components

#![cfg_attr(test, allow(dead_code, unused_imports))]
#![cfg_attr(not(test), no_main, no_std)]
#![feature(
	alloc_error_handler,
	decl_macro,
	exclusive_range_pattern,
	ptr_internals,
	stmt_expr_attributes
)]
#![allow(soft_unstable)]

extern crate alloc;
#[macro_use]
extern crate lazy_static;

mod arch;
mod bootboot;
mod descriptors;
mod drivers;
mod graphx;
mod interrupts;
mod io;
mod memory;
mod panic;
mod utils;

use core::arch::asm;

use crate::{
	drivers::{acpi, tty::print},
	interrupts::HALT,
	utils::logger::{
		kerr, kinf, log,
		Color::{BrightMagenta, Clear},
	},
};

use spin::Mutex;

static NEEDS_INIT: Mutex<bool> = Mutex::new(true);

/// Kernel entry point
#[no_mangle]
fn _start() {
	let mut needs_init = NEEDS_INIT.lock();

	use bootboot::*;
	use memory::heap::allocator;

	let bootboot_r = unsafe { &(*(BOOTBOOT_INFO as *const Bootboot)) };

	if bootboot_r.fb_scanline != 0 {
		if *needs_init {
			allocator::init();
			graphx::katyperry::show();
			graphx::swap_buffers();
			drivers::tty::init();

			log!("{BrightMagenta}Booting KatyPerryOS{Clear}");

			kinf!(
				"Screen size: {}x{}",
				graphx::get_screen_width(),
				graphx::get_screen_height()
			);

			utils::cpudet::detect_cpu();

			kinf!("Number of CPU cores: {}", { bootboot_r.numcores });

			memory::paging::init();

			let kheap_info = allocator::info();

			kinf!(
				"Kernel heap is at 0x{:x}, size: {} bytes",
				kheap_info.0 as usize,
				kheap_info.1
			);

			descriptors::gdt::init();
			descriptors::idt::init();
			interrupts::pit::init(1000);
			drivers::acpi::init();
			drivers::acpi::enable();
			drivers::rtc::init(13);
			drivers::peripheral::serial::init();
			drivers::peripheral::parallel::init();
			drivers::peripheral::pci::init();

			if let Err(msg) = drivers::ps2::init() {
				kerr!("Couldn't initialize PS/2 ports: {msg}");
			}

			drivers::ps2::keyboard::register_handler(
				|active_keys: &drivers::ps2::keyboard::ActiveKeys| {
					if active_keys.active_key.key_code == 1 {
						acpi::shut_down();
					}

					if active_keys.active_key.pressed
						&& active_keys.active_key.char_code != 0
					{
						print!("{}", active_keys.active_key.char_code as char);
					}
				},
			);

			unsafe {
				interrupts::enable();
			}

			*needs_init = false;
		} else {
			// Some dummy stuff to wake the core
			unsafe {
				asm!("nop");
			}
		}

		drop(needs_init);

		loop {
			unsafe { HALT!() };
		}
	} else {
		panic!();
	}
}
