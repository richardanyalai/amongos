use core::arch::global_asm;

global_asm!(include_str!("x86_64/gdt.S"));
global_asm!(include_str!("x86_64/int_stubs.S"));
