//! Constants and functions for Interrupt Requests

#![allow(dead_code)]

use crate::{interrupts::pic::send_eoi, utils::logger::kerr};

// Master 8259:

/// PIT
pub const IRQ_0: u8 = 32;

/// Keyboard
pub const IRQ_1: u8 = 33;

/// 8259A slave controller
pub const IRQ_2: u8 = 34;

/// COM2 / COM4
pub const IRQ_3: u8 = 35;

/// COM1 / COM3
pub const IRQ_4: u8 = 36;

/// LPT2
pub const IRQ_5: u8 = 37;

/// Floppy controller
pub const IRQ_6: u8 = 38;

/// LPT1
pub const IRQ_7: u8 = 39;

// Slave 8259:

/// RTC
pub const IRQ_8: u8 = 40;

/// Unassigned
pub const IRQ_9: u8 = 41;

/// Unassigned
pub const IRQ_10: u8 = 42;

/// Unassigned
pub const IRQ_11: u8 = 43;

/// Mouse
pub const IRQ_12: u8 = 44;

/// Math coprocessor
pub const IRQ_13: u8 = 45;

/// Hard disk controller 1
pub const IRQ_14: u8 = 46;

/// Hard disk controller 2
pub const IRQ_15: u8 = 47;

/// Interrupt Request handler method
///
/// * `regs` - a pointer to the structure containing the registers that were
/// pushed to the stack in *src/asm/x86_64/int_stubs.S*
///
/// For more details please read:
/// * <https://wiki.osdev.org/Interrupts#Standard_ISA_IRQs>
/// * <http://www.jamesmolloy.co.uk/tutorial_html/5.-IRQs%20and%20the%20PIT.html>
#[no_mangle]
pub fn irq_handler(regs: &crate::io::registers::registers) {
	let int_handler = &(*super::INTERRUPT_HANDLERS.lock())[regs.int as usize];

	if int_handler.set {
		unsafe {
			(int_handler.handler)();
		}
	} else {
		kerr!("UNHANDLED IRQ {}", regs.int - 32);
	}

	unsafe {
		send_eoi(regs.int as u8);
	}
}
