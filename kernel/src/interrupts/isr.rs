//! Constants and functions for Interrupt Service Routines

#![allow(dead_code)]

use alloc::{format, string::String};
use core::arch::asm;

use crate::{
	drivers::tty::{clear_screen, tty_set_bgcolor, tty_set_fgcolor},
	graphx::{
		color::*, draw_fillrect, get_screen_height, get_screen_width,
		swap_buffers,
	},
	interrupts::HALT,
	utils::logger::log,
};

/// Divide by 0
pub const ISR_0: u8 = 0;

/// Reserved
pub const ISR_1: u8 = 1;

/// NMI Interrupt
pub const ISR_2: u8 = 2;

/// Breakpoint
pub const ISR_3: u8 = 3;

/// Overflow
pub const ISR_4: u8 = 4;

/// Bounds range exceeded (BOUND)
pub const ISR_5: u8 = 5;

/// Invalid opcode (UD2)
pub const ISR_6: u8 = 6;

/// Device not available (WAIT/FWAIT)
pub const ISR_7: u8 = 7;

/// Double fault
pub const ISR_8: u8 = 8;

/// Coprocessor segment overrun
pub const ISR_9: u8 = 9;

/// Invalid TSS
pub const ISR_10: u8 = 10;

/// Segment not present
pub const ISR_11: u8 = 11;

/// Stack-segment fault
pub const ISR_12: u8 = 12;

/// General protection fault
pub const ISR_13: u8 = 13;

/// Page fault
pub const ISR_14: u8 = 14;

/// Reserved
pub const ISR_15: u8 = 15;

/// x87 FPU error
pub const ISR_16: u8 = 16;

/// Alignment check
pub const ISR_17: u8 = 17;

/// Machine check
pub const ISR_18: u8 = 18;

/// SIMD Floating-Point Exception
pub const ISR_19: u8 = 19;

// 20 - 31 : Reserved
pub const ISR_20: u8 = 20;
pub const ISR_21: u8 = 21;
pub const ISR_22: u8 = 22;
pub const ISR_23: u8 = 23;
pub const ISR_24: u8 = 24;
pub const ISR_25: u8 = 25;
pub const ISR_26: u8 = 26;
pub const ISR_27: u8 = 27;
pub const ISR_28: u8 = 28;
pub const ISR_29: u8 = 29;
pub const ISR_30: u8 = 30;
pub const ISR_31: u8 = 31;

// 32 - 255: User definable
pub const ISR_128: u8 = 128;

static EXCEPTION_MESSAGES: [&str; 32] = [
	"DIVISION BY ZERO",
	"DEBUG",
	"NON-MASKABLE INTERUPT",
	"BREAKPOINT",
	"DETECTED OVERFLOW",
	"OUT-OF-BOUNDS",
	"INVALID OPCODE",
	"NO COPROCESSOR",
	"DOUBLE FAULT",
	"COPROCESSOR SEGMENT OVERRUN",
	"BAD TSS",
	"SEGMENT NOT PRESENT",
	"STACK FAULT",
	"GENERAL PROTECTION FAULT",
	"PAGE FAULT",
	"UNKNOWN INTERRUPT",
	"COPROCESSOR FAULT",
	"ALIGNMENT CHECK",
	"MACHINE CHECK",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
];

/// Interrupt Service Routine handler method
///
/// * `regs` - a pointer to the structure containing the registers that were
/// pushed to the stack in *src/asm/x86_64/int_stubs.S*
///
/// For more details please read:
/// * <https://wiki.osdev.org/Interrupts_tutorial#ISRs>
/// * <https://wiki.osdev.org/Interrupt_Service_Routines>
/// * <http://www.jamesmolloy.co.uk/tutorial_html/4.-The%20GDT%20and%20IDT.html>
#[no_mangle]
#[rustfmt::skip]
pub fn isr_handler(regs: &crate::io::registers::registers) {
	let int_handler = &(*super::INTERRUPT_HANDLERS.lock())[regs.int as usize];

	if int_handler.set {
		unsafe { (int_handler.handler)(); }
	} else {
		clear_screen!();

		draw_fillrect(
			0,
			0,
			get_screen_width() as u16,
			get_screen_height() as u16,
			Colors::Blue as u32,
		);

		swap_buffers();

		tty_set_bgcolor!(Colors::Blue);
		tty_set_fgcolor!(Colors::White);

		log!(
			"KERNEL PANIC: {}\n\n\
			{}\
			ERROR CODE: {:04x}\n\n\
			RAX={:016x} RBX={:016x} RCX={:016x} RDX={:016x}\n\
			RSI={:016x} RDI={:016x} RBP={:016x} RSP={:016x}\n\
			R08={:016x} R09={:016x} R10={:016x} R11={:016x}\n\
			R12={:016x} R13={:016x} R14={:016x} R15={:016x}\n\
			RIP={:016x} RFL={:08x}\n\
			ES ={:04x}\n\
			CS ={:04x}\n\
			SS ={:04x}\n\
			DS ={:04x}\n\
			FS ={:04x}\n\
			GS ={:04x}\n\
			GDT={:16x}\n\
			IDT={:16x}",
			EXCEPTION_MESSAGES[regs.int as usize],
			if regs.int == ISR_14 as u64 {
				let cr2: u32;

				unsafe { asm!("mov {0:r}, cr2", out(reg) cr2); }

				format!("ACCESSED ADDRESS: 0x{:016x}\n\n", cr2)
			} else {
				String::new()
			},
			regs.err,
			regs.rax, regs.rbx, regs.rcx, regs.rdx,
			regs.rsi, regs.rdi, regs.rbp, regs.rsp,
			regs.r08, regs.r09, regs.r10, regs.r11,
			regs.r12, regs.r13, regs.r14, regs.r15,
			regs.rip, regs.rfl,
			regs.ds,
			regs.cs,
			regs.ss,
			regs.ds,
			regs.ds,
			regs.ds,
			crate::descriptors::gdt::sgdt(),
			crate::descriptors::idt::sidt(),
		);

		loop {
			unsafe { HALT!(); }
		}
	}
}
