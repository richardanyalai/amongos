//! Constants and functions for the Pogrammable Interrupt Controller

use crate::io::ports::{io_wait, outb};

// PIC port constants

/// Master Command Port
pub const PIC_MCP: u16 = 0x20;
/// Master Data Port
pub const PIC_MDP: u16 = 0x21;
/// Slave Command Port
pub const PIC_SCP: u16 = 0xA0;
/// Slave Data Port
pub const PIC_SDP: u16 = 0xA1;

/// Interrupt received code
const PIC_EOI: u8 = 0x20;

/// Remap the Pogrammable Interrupt Controller
/// 
/// # Safety
/// 
/// This function is unsafe because uses unsafe functions for hardware
/// communication.
#[rustfmt::skip]
pub unsafe fn remap() {
	// Remapping the PIC
	outb(PIC_MCP, 0x11);	// Write ICW1 to PICM, we are gonna write
							// commands to PICM
	io_wait();
	outb(PIC_SCP, 0x11);	// Write ICW1 to PICS, we are gonna write
							// commands to PICS
	io_wait();
	outb(PIC_MDP, 0x20);	// Remap PICM to 0x20 (32 decimal)
	io_wait();
	outb(PIC_SDP, 0x28);	// Remap PICS to 0x28 (40 decimal)
	io_wait();
	outb(PIC_MDP, 0x04);	// IRQ2 -> connection to slave
	io_wait();
	outb(PIC_SDP, 0x02);
	io_wait();
	outb(PIC_MDP, 0x01);	// Write ICW4 to PICM, we are gonna write
							// commands to PICM
	io_wait();
	outb(PIC_SDP, 0x01);	// Write ICW4 to PICS, we are gonna write
							// commands to PICS
	io_wait();
	outb(PIC_MDP, 0x00);	// Enable all IRQs on PICM
	io_wait();
	outb(PIC_SDP, 0x00);	// Enable all IRQs on PICS
	io_wait();
}

/// Send end of interrupt (interrupt received)
///
/// * `int` - number of the interrupt to be acknowledge
///
/// # Safety
///
/// This function is unsafe because uses unsafe functions for hardware
/// communication.
pub unsafe fn send_eoi(int: u8) {
	use super::irq::{IRQ_0, IRQ_15, IRQ_8};

	if (IRQ_0..IRQ_15).contains(&int) {
		outb(PIC_MCP, PIC_EOI);

		if int >= IRQ_8 {
			outb(PIC_SCP, PIC_EOI);
		}
	}
}
