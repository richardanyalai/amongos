//! Functions for the Programmable Interval Timer

#![allow(dead_code)]

use crate::{
	interrupts::{irq::IRQ_0, register_interrupt_handler, HALT},
	io::ports::outb,
	utils::logger::kok,
};

use spin::Mutex;

static TICK: Mutex<u64> = Mutex::new(0);

/// Increment the tick counter by one
pub fn tick() {
	*TICK.lock() += 1;
}

/// Get the current number of ticks
pub fn get_ticks() -> u64 {
	*TICK.lock()
}

/// Sleep for the time given in milliseconds
///
/// * `millis` - the amount of time in milliseconds
pub fn _sleep(millis: u64) {
	let end = get_ticks() + millis;

	while end >= get_ticks() {
		unsafe {
			HALT!();
		}
	}
}

/// Initialize the Programmable Interval Timer
///
/// * `freq` - frequency of the timer
pub fn init(freq: u64) {
	let divisor = 1193180 / freq;

	unsafe {
		outb(0x43, 0x36);
		outb(0x40, (divisor & 0xFF) as u8);
		outb(0x40, ((divisor >> 8) & 0xFF) as u8);
	}

	register_interrupt_handler(IRQ_0, true, tick);

	kok!("Initialized PIT");
}

pub macro sleep($time:expr) {
	crate::interrupts::pit::_sleep($time)
}
