//! Functions for interrupt handling

pub mod irq;
pub mod isr;
pub mod pic;
pub mod pit;

use core::arch::asm;

use spin::Mutex;

/// Struct representing an interrupt handler
/// 
/// * `set`     - whether the handler has been set
/// * `handler` - handler function
#[rustfmt::skip]
#[derive(Clone, Copy)]
pub struct InterruptHandler {
	handler: unsafe fn(),
	set:     bool,
}

/// Array containing interrupt handler structs
static INTERRUPT_HANDLERS: Mutex<[InterruptHandler; 256]> = Mutex::new(
	[InterruptHandler {
		handler: || {},
		set: false,
	}; 256],
);

/// Register an interrupt handler
///
/// * `int_no`  - number of the interrupt
/// * `set`     - set the handler if true, remove otherwise
/// * `handler` - handler function
#[rustfmt::skip]
pub fn register_interrupt_handler(
	int_no : u8,
	set    : bool,
	handler: unsafe fn(),
) {
	(*INTERRUPT_HANDLERS.lock())[int_no as usize] = InterruptHandler {
		handler, set
	};
}

/// Tells whether interrupts are enabled or not
///
/// # Safety
///
/// This function is unsafe because uses inline assembly
pub unsafe fn enabled() -> bool {
	let mut flags: u32;

	asm!(
		"pushf
		pop {0:r}",
		out(reg) flags
	);

	(flags & (1 << 9)) != 0
}

/// Enables interrupts
///
/// # Safety
///
/// This function is unsafe because uses inline assembly
pub unsafe fn enable() {
	asm!("sti", options(nomem, nostack));
}

/// Disables interruts
///
/// # Safety
///
/// This function is unsafe because uses inline assembly
#[allow(dead_code)]
pub unsafe fn disable() {
	asm!("cli", options(nomem, nostack));
}

pub macro HALT() {
	asm!("hlt", options(nomem, nostack))
}
