//! Structure representing CPU registers

/// A structure containing CPU register values pushed by the interrupt handler
/// stubs in *src/asm/x86_64/int_stubs.S*
#[rustfmt::skip]
#[repr(C)]
pub struct registers {
	// Data segment selector
	pub ds    : u64,

	// Pushed by pusha
	pub r15   : u64,
	pub r14   : u64,
	pub r13   : u64,
	pub r12   : u64,
	pub r11   : u64,
	pub r10   : u64,
	pub r09   : u64,
	pub r08   : u64,
	pub rsi   : u64,
	pub rdi   : u64,
	pub rbp   : u64,
	pub rdx   : u64,
	pub rcx   : u64,
	pub rbx   : u64,
	pub rax   : u64,

	// Interrupt number and error code (if applicable)
	pub int   : u64,
	pub err   : u64,

	// Pushed by the processor automatically
	pub rip   : u64,
	pub cs    : u64,
	pub rfl   : u64,
	pub rsp   : u64,
	pub ss    : u64,
}
