//! Wrappers around 8, 16 and 32-bit port IO

#![allow(dead_code)]

use core::arch::asm;

/// Read 8 bits of data
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn inb(port: u16) -> u8 {
	let mut value: u8;

	asm!(
		"in al, dx", out("al") value, in("dx") port,
		options(nomem, nostack)
	);

	value
}

/// Read 16 bits of data
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn inw(port: u16) -> u16 {
	let mut value: u16;

	asm!(
		"in ax, dx", out("ax") value, in("dx") port,
		options(nomem, nostack)
	);

	value
}

/// Read 32 bits of data
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn inl(port: u16) -> u32 {
	let mut value: u32;

	asm!(
		"in eax, dx", out("eax") value, in("dx") port,
		options(nomem, nostack)
	);

	value
}

/// Write 8 bits of data
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn outb(port: u16, value: u8) {
	asm!(
		"out dx, al", in("dx") port, in("al") value,
		options(nomem, nostack)
	);
}

/// Write 16 bits of data
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn outw(port: u16, value: u16) {
	asm!(
		"out dx, ax", in("dx") port, in("ax") value,
		options(nomem, nostack)
	);
}

/// Write 32 bits of data
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn outl(port: u16, value: u32) {
	asm!(
		"out dx, eax", in("dx") port, in("eax") value,
		options(nomem, nostack)
	);
}

/// Wait for I/O operations to complete
///
/// # Safety
///
/// This function is unsafe because uses inline assembly for I/O operations.
#[inline]
pub unsafe fn io_wait() {
	outb(0x80, 0x00);
}
