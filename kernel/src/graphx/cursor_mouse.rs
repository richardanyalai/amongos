use alloc::{vec, vec::Vec};

use crate::graphx::{color::Colors::*, *};

use spin::Mutex;

lazy_static! {
	static ref MOUSE_BUFFER: Mutex<Vec<u32>> = Mutex::new(vec![
		0;
		(get_screen_width() * get_screen_height())
			as usize
	]);
}

pub fn clear_old(x: u16, y: u16) {
	let width = get_screen_width() as usize;
	let height = get_screen_height() as usize;

	for _y in -1..19 {
		for _x in -1..14 {
			let x = x as i16 + _x;
			let y = y as i16 + _y;

			if y < height as i16 && x < width as i16 && x > 0 {
				let x = x as usize;
				let y = y as usize;
				let offset = width * y + x;
				let color = (*BACK_BUFFER.lock())[offset];

				draw_pixel_directly(x as u16, y as u16, color);
			}
		}
	}
}

pub fn draw_cursor(x: u16, y: u16) {
	let width = get_screen_width() as usize;
	let height = get_screen_height() as usize;

	for _y in 0..18 {
		for _x in 0..13 {
			let x = x as i16 + _x;
			let y = y as i16 + _y;

			if CURSOR[_y as usize][_x as usize] < 2
				&& y < height as i16
				&& x < width as i16
				&& x > 0
			{
				draw_pixel_directly(
					x as u16,
					y as u16,
					if CURSOR[_y as usize][_x as usize] == 1 {
						Pink
					} else {
						Magenta
					} as u32,
				);
			}
		}
	}
}

#[rustfmt::skip]
const CURSOR: [[u32; 13]; 18] = [
	[1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
	[1, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
	[1, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2],
	[1, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2],
	[1, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2],
	[1, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2],
	[1, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2],
	[1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2],
	[1, 0, 0, 0, 1, 1, 0, 0, 1, 2, 2, 2, 2],
	[1, 0, 0, 1, 2, 1, 0, 0, 1, 2, 2, 2, 2],
	[1, 0, 1, 2, 2, 2, 1, 0, 0, 1, 2, 2, 2],
	[1, 0, 1, 2, 2, 2, 1, 0, 0, 1, 2, 2, 2],
	[1, 1, 2, 2, 2, 2, 2, 1, 0, 0, 1, 2, 2],
	[2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2],
];
