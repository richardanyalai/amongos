//! Constants, structures and functions for representing colors

#![allow(dead_code)]

/// Standard CGA 16-color palette
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
#[rustfmt::skip]
pub enum Colors {
	Black      = 0x000000, //  0
	Blue       = 0x0000AA, //  1
	Green      = 0x00AA00, //  2
	Cyan       = 0x00AAAA, //  3
	Red        = 0xAA0000, //  4
	Magenta    = 0xAA00AA, //  5
	Brown      = 0xAA5500, //  6
	LightGray  = 0xAAAAAA, //  7
	DarkGray   = 0x555555, //  8
	LightBlue  = 0x5555FF, //  9
	LightGreen = 0x55FF55, // 10
	LightCyan  = 0x55FFFF, // 11
	LightRed   = 0xFF5555, // 12
	Pink       = 0xFF55FF, // 13
	Yellow     = 0xFFFF55, // 14
	White      = 0xFFFFFF, // 15
}

/// A struct representing ARGB colors
///
/// * `a` - *alpha* channel
/// * `r` - *red* channel
/// * `g` - *green* channel
/// * `b` - *blue* channel
#[derive(Copy, Clone)]
pub struct Color {
	a: u8,
	r: u8,
	g: u8,
	b: u8,
}

#[rustfmt::skip]
impl Color {
	/// Create a color from the given color channel values
	///
	/// * `a` - *alpha* channel
	/// * `r` - *red* channel
	/// * `g` - *green* channel
	/// * `b` - *blue* channel
	pub fn new(a: u8, r: u8, g: u8, b: u8) -> Color { Color { a, r, g, b } }

	/// Returns a color struct created using the given hex color code
	///
	/// - `color` - color code
	pub fn new_hex(&mut self, color: u32) -> Color {
		Color::new(
			((color >> 24) & 0xFF) as u8,
			((color >> 16) & 0xFF) as u8,
			((color >>  8) & 0xFF) as u8,
			(color         & 0xFF) as u8
		)
	}

	// Setters for color channels

	pub fn set_a(&mut self, a: u8) -> &mut Color { self.a = a; self }
	pub fn set_r(&mut self, r: u8) -> &mut Color { self.r = r; self }
	pub fn set_g(&mut self, g: u8) -> &mut Color { self.g = g; self }
	pub fn set_b(&mut self, b: u8) -> &mut Color { self.b = b; self }

	// Getters for color channels

	pub fn get_a(self) -> u8 { self.a }
	pub fn get_r(self) -> u8 { self.r }
	pub fn get_g(self) -> u8 { self.g }
	pub fn get_b(self) -> u8 { self.b }

	/// Returns color code created from the values of the color channels
	#[rustfmt::skip]
	pub fn get_value(self) -> u32 {
		((self.a as u32) << 24)
	  + ((self.r as u32) << 16)
	  + ((self.g as u32) <<  8)
	  +  (self.b as u32)
	}
}
