//! Basic graphics functions

#![allow(dead_code)]

pub mod color;
pub mod cursor_mouse;
pub mod font;
pub mod katyperry;

use alloc::{vec, vec::Vec};

use crate::bootboot;

use spin::Mutex;

lazy_static! {
	static ref BACK_BUFFER: Mutex<Vec<u32>> = Mutex::new(vec![
		0;
		(get_screen_width() * get_screen_height())
			as usize
	]);
}

/// Returns the Bootboot structure
pub fn get_bootboot() -> &'static bootboot::Bootboot {
	unsafe { &(*(bootboot::BOOTBOOT_INFO as *const bootboot::Bootboot)) }
}

/// Returns the width of the framebuffer
pub fn get_screen_width() -> u32 {
	get_bootboot().fb_width
}

/// Returns the height of the framebuffer
pub fn get_screen_height() -> u32 {
	get_bootboot().fb_height
}

/// Returns the width of the font
pub fn get_font_width() -> u32 {
	font::FONT_WIDTH as u32
}

/// Returns the height of the font
pub fn get_font_height() -> u32 {
	font::FONT_HEIGHT as u32
}

/// Draw a pixel directly to video ram
///
/// * `x` - *X* coordinate
/// * `y` - *Y* coordinate
/// * `color` - color of the pixel
#[inline]
pub fn draw_pixel_directly(x: u16, y: u16, color: u32) {
	let fb = get_bootboot().fb_ptr;
	let scanline = get_bootboot().fb_scanline;

	unsafe {
		let offset = scanline as u64 * y as u64 + x as u64 * 4;
		*((fb as u64 + offset) as *mut u32) = color;
	}
}

/// Draw a pixel to the back buffer
///
/// * `x` - *X* coordinate
/// * `y` - *Y* coordinate
/// * `color` - color of the pixel
pub fn draw_pixel(x: u16, y: u16, color: u32) {
	let offset = get_screen_width() as usize * y as usize + x as usize;
	(*BACK_BUFFER.lock())[offset] = color;
}

/// Draw a filled rectangle
///
/// * `x` - *X* coordinate
/// * `y` - *Y* coordinate
/// * `w` - width
/// * `h` - height
/// * `color` - color of the rectangle
pub fn draw_fillrect(x: u16, y: u16, w: u16, h: u16, color: u32) {
	for _y in y..y + h {
		for _x in x..x + w {
			draw_pixel(_x, _y, color);
		}
	}
}

/// Draw a character
///
/// * `x` - *X* coordinate
/// * `y` - *Y* coordinate
/// * `draw_bg` - whether the text background should be drawn
/// * `bg` - background color
/// * `fg` - foreground color
/// * `s` - the character to be drawn
pub fn draw_char(x: u16, y: u16, draw_bg: bool, bg: u32, fg: u32, s: u8) {
	for _x in 0..8 {
		for _y in 0..16 {
			if font::FONT[s as usize * 16 + _y] >> (7 - _x) & 1 == 1 {
				draw_pixel_directly(x + _x as u16, y + _y as u16, fg);
				draw_pixel(x + _x as u16, y + _y as u16, fg);
			} else if draw_bg {
				draw_pixel_directly(x + _x as u16, y + _y as u16, bg);
				draw_pixel(x + _x as u16, y + _y as u16, fg);
			}
		}
	}
}

/// Copies back buffer to video ram
pub fn swap_buffers() {
	unsafe {
		core::ptr::copy_nonoverlapping(
			BACK_BUFFER.lock().as_ptr() as *const u8,
			get_bootboot().fb_ptr,
			(get_screen_height() * get_screen_width() * 4) as usize,
		);
	}
}

/// Fills back buffer with zeroes
pub fn clear_screen() {
	BACK_BUFFER.lock().fill(0);
}
