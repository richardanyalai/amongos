//! Structures and functions for the Interrupt Descriptor Table

use core::arch::asm;

use crate::{
	interrupts::{irq::*, isr::*},
	utils::logger::kok,
};

use spin::Mutex;

/// IDT "pointer" structure
/// 
/// * `limit` - the length of the IDT in bytes - 1
/// (minimum value is 100h, a value of 1000h means 200h interrupts)
/// * `offset` - the linear address where the IDT starts (INT 0)
/// 
/// For more details please read
/// <https://wiki.osdev.org/Interrupt_Descriptor_Table#Location_and_Size>
#[rustfmt::skip]
#[repr(C, packed)]
pub struct IdtPtr {
	pub limit : u16,
	pub offset: u64,
}

/// IDT "pointer" structure
/// 
/// * `offset` - offset bits 0..15
/// * `selector` - code segment selector in GDT or LDT
/// * `ist` - bits 0..2 holds Interrupt Stack Table offset, rest of bits zero
/// * `type_attr` - type and attributes
/// * `offset1` - offset bits 16..31
/// * `offset2` - offset bits 32..63
/// * `zero` - reserved
/// 
/// For more details please read:
/// * <https://wiki.osdev.org/Interrupt_Descriptor_Table#IDT_in_IA-32e_Mode_.2864-bit_IDT.29>
/// * <http://www.jamesmolloy.co.uk/tutorial_html/4.-The%20GDT%20and%20IDT.html>
#[rustfmt::skip]
#[derive(Copy, Clone, Default)]
#[repr(C, packed)]
struct IdtEntry {
	pub offset0  : u16,
	pub selector : u16,
	pub ist      : u8,
	pub type_attr: u8,
	pub offset1  : u16,
	pub offset2  : u32,
	pub zero     : u32,
}

lazy_static! {
	/// An array which represents the IDT
	static ref IDT_ENTRIES: Mutex<[IdtEntry; 256]> =
		Mutex::new([IdtEntry::default(); 256]);
}

/// A function for setting up an IDT entry
/// 
/// * `entry_offset` - index of the IDT entry
/// * `handler` - interrupt handler function
/// * `type_attr` - access flags
/// * `selector` - code segment selector
/// 
/// The interrupt handler stub functions get defined in
/// *asm/x86_64/int_stubs.S*
/// 
/// # Safety
/// 
/// This function is unsafe because it modifies the fields of an entry in a
/// static mtable array.
#[rustfmt::skip]
fn set_gate(
	entry_offset: u8,
	offset      : u64,
	type_attr   : u8,
	selector    : u16,
) {
	let i = entry_offset as usize;
	let idt_entries = &mut (*IDT_ENTRIES.lock());

	idt_entries[i].offset0   =  (offset  & 0x000000000000ffff)        as u16;
	idt_entries[i].offset1   = ((offset  & 0x00000000ffff0000) >> 16) as u16;
	idt_entries[i].offset2   = ((offset  & 0xffffffff00000000) >> 32) as u32;
	idt_entries[i].type_attr = type_attr | 0x60;
	idt_entries[i].selector  = selector;
}

/// A macro for creating and installing interrupt handlers
///
/// * `int_no` - interrupt number
/// * `func_name` - name of the function being generated
macro define_handler($int_no:expr, $func_name:ident) {
	extern "C" {
		fn $func_name();
	}

	set_gate($int_no, $func_name as *const fn() as u64, 0x8E, 0x08);
}

/// Get the address of the current IDT
#[inline]
pub fn sidt() -> u64 {
	let mut idt: IdtPtr = IdtPtr {
		limit: 0,
		offset: 0,
	};

	unsafe {
		asm!("sidt [{}]", in(reg) &mut idt, options(nostack, preserves_flags));
	}

	idt.offset
}

/// Initialize the IDT entries and load the IDT
/// 
/// # Safety
/// 
/// This function is unsafe because it calls multiple unsafe functions and uses
/// inline assembly.
#[rustfmt::skip]
pub fn init() {
	// ISRs and exceptions
	define_handler!(ISR_0,     isr0);
	define_handler!(ISR_1,     isr1);
	define_handler!(ISR_2,     isr2);
	define_handler!(ISR_3,     isr3);
	define_handler!(ISR_4,     isr4);
	define_handler!(ISR_5,     isr5);
	define_handler!(ISR_6,     isr6);
	define_handler!(ISR_7,     isr7);
	define_handler!(ISR_8,     isr8);
	define_handler!(ISR_9,     isr9);
	define_handler!(ISR_10,   isr10);
	define_handler!(ISR_11,   isr11);
	define_handler!(ISR_12,   isr12);
	define_handler!(ISR_13,   isr13);
	define_handler!(ISR_14,   isr14);
	define_handler!(ISR_15,   isr15);
	define_handler!(ISR_16,   isr16);
	define_handler!(ISR_17,   isr17);
	define_handler!(ISR_18,   isr18);
	define_handler!(ISR_19,   isr19);
	define_handler!(ISR_20,   isr20);
	define_handler!(ISR_21,   isr21);
	define_handler!(ISR_22,   isr22);
	define_handler!(ISR_23,   isr23);
	define_handler!(ISR_24,   isr24);
	define_handler!(ISR_25,   isr25);
	define_handler!(ISR_26,   isr26);
	define_handler!(ISR_27,   isr27);
	define_handler!(ISR_28,   isr28);
	define_handler!(ISR_29,   isr29);
	define_handler!(ISR_30,   isr30);
	define_handler!(ISR_31,   isr31);
	define_handler!(ISR_128, isr128);

	// IRQs
	define_handler!(IRQ_0,     irq0);
	define_handler!(IRQ_1,     irq1);
	define_handler!(IRQ_2,     irq2);
	define_handler!(IRQ_3,     irq3);
	define_handler!(IRQ_4,     irq4);
	define_handler!(IRQ_5,     irq5);
	define_handler!(IRQ_6,     irq6);
	define_handler!(IRQ_7,     irq7);
	define_handler!(IRQ_8,     irq8);
	define_handler!(IRQ_9,     irq9);
	define_handler!(IRQ_10,   irq10);
	define_handler!(IRQ_11,   irq11);
	define_handler!(IRQ_12,   irq12);
	define_handler!(IRQ_13,   irq13);
	define_handler!(IRQ_14,   irq14);
	define_handler!(IRQ_15,   irq15);

	// Load IDT
	unsafe {
		asm!("lidt [{}]", in(reg) &IdtPtr {
			limit : core::mem::size_of::<[IdtEntry; 256]>() as u16 - 1,
			offset: &*IDT_ENTRIES.lock() as *const [IdtEntry; 256] as u64,
		}, options(readonly, nostack, preserves_flags));

		kok!("Loaded IDT at {:#16x}", sidt());

		// Remap the PIC
		crate::interrupts::pic::remap();
	}
}
