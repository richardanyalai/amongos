//! Structures and functions for the Global Descriptor Table

#![allow(dead_code)]

use core::arch::asm;

use crate::utils::logger::kok;

use spin::Mutex;

/// GDT "pointer" structure
/// 
/// * `size` - the size of the table subtracted by 1 (up to 65536 bytes, a
/// maximum of 8192 entries)
/// * `offset` - the linear address of the table itself
/// 
/// For more details please read
/// <https://wiki.osdev.org/Global_Descriptor_Table#Structure>
#[rustfmt::skip]
#[repr(C, packed)]
pub struct GdtPtr {
	pub size  : u16,
	pub offset: u64,
}

/// GDT entry structure
/// 
/// Represents an entry in the Global Descriptor table.
/// 
/// * `limit_lo` - the lower 16 bits of the limit
/// * `base_lo` - the lower 16 bits of the base
/// * `base_mid` - the next 8 bits of the base
/// * `access` - access flags, determine what ring this segment can be used in
/// * `base_hi` - the last 8 bits of the base
/// 
/// For more details please read:
/// * <https://wiki.osdev.org/Global_Descriptor_Table>
/// * <http://www.jamesmolloy.co.uk/tutorial_html/4.-The%20GDT%20and%20IDT.html>
#[rustfmt::skip]
#[repr(C, packed)]
#[derive(Copy, Clone, Default)]
struct GdtEntry {
	pub limit_lo   : u16,
	pub base_lo    : u16,
	pub base_mid   : u8,
	pub access     : u8,
	pub granularity: u8,
	pub base_hi    : u8,
}

lazy_static! {
	/// An array which represents the GDT
	static ref GDT_ENTRIES: Mutex<[GdtEntry; 5]> =
		Mutex::new([GdtEntry::default(); 5]);
}

/// Set the values of a GDT entry
/// 
/// # Safety
/// 
/// This function is unsafe because it modifies the fields of an entry in a
/// static mtable array.
#[rustfmt::skip]
fn set_entry(
	index      : usize,
	limit_lo   : u16,
	base_lo    : u16,
	base_mid   : u8,
	access     : u8,
	granularity: u8,
	base_hi    : u8,
) {
	(*GDT_ENTRIES.lock())[index] = GdtEntry {
		limit_lo,
		base_lo,
		base_mid,
		access,
		granularity,
		base_hi,
	};
}

/// Get the address of the current GDT
#[inline]
pub fn sgdt() -> u64 {
	let mut gdt: GdtPtr = GdtPtr { size: 0, offset: 0 };

	unsafe {
		asm!("sgdt [{}]", in(reg) &mut gdt, options(nostack, preserves_flags));
	}

	gdt.offset
}

extern "C" {
	fn flush_gdt(gdt: u64);
}

/// Initialize the GDT entries and load the GDT
///
/// # Safety
///
/// This function is unsafe because it calls an unsafe function multiple times
/// and an  extern "C" unsafe function once.
pub fn init() {
	set_entry(0, 0, 0, 0, 0x00, 0x00, 0); // Null segment
	set_entry(1, 0, 0, 0, 0x9A, 0xA0, 0); // Kernel code segment
	set_entry(2, 0, 0, 0, 0x92, 0x00, 0); // Kernel data segment
	set_entry(3, 0, 0, 0, 0xFA, 0xA0, 0); // User code segment
	set_entry(4, 0, 0, 0, 0xF2, 0x00, 0); // User data segment

	unsafe {
		flush_gdt(&GdtPtr {
			size: core::mem::size_of::<[GdtEntry; 5]>() as u16 - 1,
			offset: &*GDT_ENTRIES.lock() as *const [GdtEntry; 5] as u64,
		} as *const GdtPtr as u64);
	}

	kok!("Loaded GDT at {:#16x}", sgdt());
}
