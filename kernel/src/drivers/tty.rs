//! Structure and functions for the text console

#![allow(dead_code)]

use alloc::{vec, vec::Vec};
use core::fmt::{Arguments, Result, Write};

use crate::graphx::{color::Colors::*, *};

use spin::Mutex;

pub macro tty_set_bgcolor($color:expr) {
	$crate::drivers::tty::_set_bg_color($color as u32)
}

pub macro tty_set_fgcolor($color:expr) {
	$crate::drivers::tty::_set_fg_color($color as u32)
}

pub macro print {
	($($arg:tt)*) => {
		$crate::drivers::tty::_print(format_args!($($arg)*))
	}
}

pub macro println {
	() => (print!("\n")),
	($($arg:tt)*) => (print!("{}\n", format_args!($($arg)*)))
}

pub macro clear_screen() {
	$crate::drivers::tty::_clear_screen()
}

/// A struct representing a TTY back buffer entry
///
/// - `ch` - the character byte
/// - `bg` - background color
/// - `fg` - foreground color
#[derive(Clone, Copy, Default)]
struct BufferEntry {
	ch: u8,
	bg: u32,
	fg: u32,
}

/// Structure for TTY writer
/// 
/// * `max_columns` - maximum number of columns
/// * `max_rows` - maximum number of rows
/// * `column` - current column index
/// * `row` - current row index
/// * `background` - background color
/// * `foreground` - foreground color
/// * `draw_bg` - whether to draw text background
#[rustfmt::skip]
#[derive(Default)]
pub struct Writer {
	max_columns: u16,
	max_rows   : u16,
	column     : u16,
	row        : u16,
	background : u32,
	foreground : u32, 
	draw_bg    : bool,
}

impl Write for Writer {
	fn write_str(&mut self, s: &str) -> Result {
		self.write_string(s);
		Ok(())
	}
}

impl Writer {
	/// Initialize writer
	#[rustfmt::skip]
	pub fn init(&mut self) {
		self.max_columns = (get_screen_width()  / get_font_width())  as u16;
		self.max_rows    = (get_screen_height() / get_font_height()) as u16;
		self.background  = Black as u32;
		self.foreground  = White as u32;

		BUFFER.lock().push(vec![]);
	}

	/// Clear TTY screen
	pub fn clear_screen(&mut self) {
		clear_screen();
		self.column = 0;
		self.row = 0;
	}

	/// Set background color
	pub fn set_bgcolor(&mut self, color: u32) {
		self.background = color;
	}

	/// Set foreground color
	pub fn set_fgcolor(&mut self, color: u32) {
		self.foreground = color;
	}

	/// Write a byte at the given coordinates
	fn write_byte_at(&mut self, x: u16, y: u16, byte: u8) {
		draw_char(x, y, self.draw_bg, self.background, self.foreground, byte);
	}

	/// Write a byte to the current position using *write_byte_at*
	fn write_byte(&mut self, byte: u8) {
		self.write_byte_at(
			self.column * get_font_width() as u16,
			self.row * get_font_height() as u16,
			byte,
		);
		self.column += 1;

		if self.column == self.max_columns - 1 {
			self.new_line();
		}

		BUFFER.lock()[self.row as usize].push(BufferEntry {
			ch: byte,
			bg: self.background,
			fg: self.foreground,
		});
	}

	/// Write a string using *write_byte*
	pub fn write_string(&mut self, string: &str) {
		let mut is_color_code = true;

		match string {
			"\x1B[0m" => {
				self.set_fgcolor(White as u32);
				self.set_bgcolor(Black as u32);
			}
			"\x1B[30m" => self.set_fgcolor(Black as u32),
			"\x1B[31m" => self.set_fgcolor(Red as u32),
			"\x1B[32m" => self.set_fgcolor(Green as u32),
			"\x1B[33m" => self.set_fgcolor(Brown as u32),
			"\x1B[34m" => self.set_fgcolor(Blue as u32),
			"\x1B[35m" => self.set_fgcolor(Magenta as u32),
			"\x1B[36m" => self.set_fgcolor(Cyan as u32),
			"\x1B[37m" => self.set_fgcolor(White as u32),
			"\x1B[90m" => self.set_fgcolor(DarkGray as u32),
			"\x1B[91m" => self.set_fgcolor(LightRed as u32),
			"\x1B[92m" => self.set_fgcolor(LightGreen as u32),
			"\x1B[93m" => self.set_fgcolor(Yellow as u32),
			"\x1B[94m" => self.set_fgcolor(LightBlue as u32),
			"\x1B[95m" => self.set_fgcolor(Pink as u32),
			"\x1B[96m" => self.set_fgcolor(LightCyan as u32),
			"\x1B[97m" => self.set_fgcolor(LightGray as u32),
			_ => is_color_code = false,
		};

		if is_color_code {
			return;
		}

		for byte in string.chars() {
			match byte {
				// Linefeed
				'\n' => {
					self.new_line();
				}
				// Carriage return
				'\r' => self.column = 0,
				// Just print that damned character
				_ => self.write_byte(byte as u8),
			}
		}
	}

	/// Print a new line and scroll the screen content if needed
	fn new_line(&mut self) {
		self.column = 0;

		if self.row == self.max_rows - 1 {
			self.scroll_screen();
		} else {
			self.row += 1;
			BUFFER.lock().push(vec![]);
		}
	}

	/// Scroll screen content up by one line
	fn scroll_screen(&mut self) {
		self.clear_screen();

		katyperry::show();

		let mut buffer = BUFFER.lock();
		buffer.remove(0);

		for (i, row) in buffer.iter().enumerate() {
			for (j, entry) in row.iter().enumerate() {
				let x = j as u16 * get_font_width() as u16;
				let y = i as u16 * get_font_height() as u16;

				self.set_bgcolor(entry.bg);
				self.set_fgcolor(entry.fg);

				self.write_byte_at(x, y, entry.ch);
			}
		}

		buffer.push(vec![]);

		self.column = 0;
		self.row = self.max_rows - 1;

		self.set_bgcolor(Black as u32);
		self.set_fgcolor(White as u32);

		crate::graphx::swap_buffers();
	}
}

lazy_static! {
	static ref WRITER: Mutex<Writer> = Mutex::new(Writer::default());
	static ref BUFFER: Mutex<Vec<Vec<BufferEntry>>> = Mutex::new(vec![]);
}

/// Global method for initializing TTY writer
pub fn init() {
	WRITER.lock().init();
}

#[doc(hidden)]
pub fn _set_bg_color(color: u32) {
	WRITER.lock().set_bgcolor(color);
}

#[doc(hidden)]
pub fn _set_fg_color(color: u32) {
	WRITER.lock().set_fgcolor(color);
}

#[doc(hidden)]
pub fn _print(args: Arguments) {
	WRITER.lock().write_fmt(args).unwrap();
}

#[doc(hidden)]
pub fn _clear_screen() {
	WRITER.lock().clear_screen();
}
