//! Structure and functions for the real-time clock (RTC, CMOS)
//!
//! https://wiki.osdev.org/Time_And_Date
//! https://www.lowlevel.eu/wiki/CMOS

#![rustfmt::skip]

use crate::{
	interrupts::{self, irq::IRQ_8},
	io::ports::{inb, outb},
	utils::logger::{kok, log},
	graphx::color::Colors::{LightGray, White},
	drivers::tty::tty_set_fgcolor
};

use spin::Mutex;

const UNIX_YEAR: u16 = 1970;

const CMOS_ADDR: u16 = 0x70;
const CMOS_DATA: u16 = 0x71;

const MONTH_DAYS: [u8; 12] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const SECONDS_IN_A_DAY: u64 = 24 * 60 * 60;

/// CMOS registers
///
/// - `Second` - CMOS register for second
/// - `Minute` - CMOS register for minute
/// - `Hour` - CMOS register for hour
/// - `WeekDay` - CMOS register for weekday
/// - `Day` - CMOS register for day of month
/// - `Month` - CMOS register for month
/// - `Year` - CMOS register for year
/// - `Century` - CMOS register for century
/// - `RegB` - "register B"
///
/// The full list of registers can be found here:
/// https://wiki.osdev.org/CMOS
enum RtcRegister {
	Second  = 0x00,
	Minute  = 0x02,
	Hour    = 0x04,
	WeekDay = 0x06,
	Day     = 0x07,
	Month   = 0x08,
	Year    = 0x09,
	Century = 0x32,
	RegB    = 0x0B,
}

/// Struct storing RTC data
///
/// - `second` - seconds
/// - `minute` - minutes
/// - `hour` - hours
/// - `week_day` - day of week
/// - `day` - day of month
/// - `month` - month
/// - `year` - year
/// - `century` - century
#[derive(Clone, Copy, Default, Debug)]
pub struct RealTimeClock {
	pub second   : u8,
	pub minute   : u8,
	pub hour     : u8,
	pub week_day : u8,
	pub day      : u8,
	pub month    : u8,
	pub year     : u8,
	pub century  : u8,
}

lazy_static! {
	static ref REAL_TIME_CLOCK: Mutex<RealTimeClock> =
		Mutex::new(RealTimeClock::default());
}

/// Update the RTC values
fn update_clock() {
	let get_update_in_progress_flag = || -> bool {
		unsafe {
			outb(CMOS_ADDR, 0x0A);
			(inb(CMOS_DATA) & 0x80) != 0
		}
	};

	let read_rtc_register = |reg: RtcRegister| -> u8 {
		unsafe {
			let tmp = inb(CMOS_ADDR);

			outb(CMOS_ADDR, (tmp & 0x80) | ((reg as u8) & 0x7F));
			inb(CMOS_DATA)
		}
	};

	while get_update_in_progress_flag() {}

	use RtcRegister::*;

	let mut rtc = REAL_TIME_CLOCK.lock();

	rtc.second   = read_rtc_register(Second);
	rtc.minute   = read_rtc_register(Minute);
	rtc.hour     = read_rtc_register(Hour);
	rtc.week_day = read_rtc_register(WeekDay);
	rtc.day      = read_rtc_register(Day);
	rtc.month    = read_rtc_register(Month);
	rtc.year     = read_rtc_register(Year);
	rtc.century  = read_rtc_register(Century);

	let register_b = read_rtc_register(RegB);

	// Convert BCD to binary values if necessary
	if (register_b & 0x04) == 0 {
		rtc.second  = (rtc.second & 0x0F) + ((rtc.second / 16) * 10);
		rtc.minute  = (rtc.minute & 0x0F) + ((rtc.minute / 16) * 10);
		rtc.hour    = ((rtc.hour & 0x0F)
			+ (((rtc.hour & 0x70_u8) / 16) * 10)) | (rtc.hour & 0x80);
		rtc.day     = (rtc.day   & 0x0F) + ((rtc.day   / 16) * 10);
		rtc.month   = (rtc.month & 0x0F) + ((rtc.month / 16) * 10);
		rtc.year    = (rtc.year  & 0x0F) + ((rtc.year  / 16) * 10);
		rtc.century = (rtc.century & 0x0F) + ((rtc.century / 16) * 10);
	}

	// Convert 12 hour clock to 24 hour clock if necessary
	if (register_b & 0x02) == 0 && (rtc.hour & 0x80) != 0 {
		rtc.hour = ((rtc.hour & 0x7F) + 12) % 24;
	}
}

/// Initialize the RTC
///
/// - `rate` - clock rate (2 < rate <= 15)
pub fn init(rate: u8) {
	unsafe {
		let enabled = interrupts::enabled();

		interrupts::disable();

		interrupts::register_interrupt_handler(IRQ_8, true, || {
			outb(CMOS_ADDR, 0x0C);
			inb(CMOS_DATA);
		});

		// Select register B and disable NMI
		outb(CMOS_ADDR, 0x8B);

		// Read the current value of register B
		let mut prev = inb(CMOS_DATA);

		// Set the index again (a read will reset the index to register D)
		outb(CMOS_ADDR, 0x8B);

		// Write the previous value. This turns on bit 6 of register B
		outb(CMOS_DATA, prev | 0x40);

		// Set index to register A, disable NMI
		outb(CMOS_ADDR, 0x8A);

		// Get initial value of register A
		prev = inb(CMOS_DATA);

		// Reset index to A
		outb(CMOS_ADDR, 0x8A);

		// Write only our rate to A. Note, rate is the bottom 4 bits.
		// Rate must be above 2 and not over 15
		outb(CMOS_DATA, (prev & 0xF0) | (rate & 0x0F));

		if enabled {
			interrupts::enable();
		}
	}

	kok!("Initialized RTC");

	tty_set_fgcolor!(LightGray);

	let clock = get_rtc_data();

	let week_day = (unix_epoch() / SECONDS_IN_A_DAY + 3) % 7;

	log!(
		"Current time is: {:02}:{:02}:{:02} {}, {}, {:02}/{:02}/{}{}",
		clock.hour,
		clock.minute,
		clock.second,
		if clock.hour < 12 { "AM" } else { "PM" },
		["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"][week_day as usize],
		clock.day,
		clock.month,
		clock.century,
		clock.year
	);

	tty_set_fgcolor!(White);
}

/// Returns RTC struct containing the current time
fn get_rtc_data() -> RealTimeClock {
	update_clock();

	*REAL_TIME_CLOCK.lock()
}

pub fn unix_epoch() -> u64 {
	let mut time = 0_u64;
	let rtc_data = get_rtc_data();

	let is_leap_year = |y| -> bool {
		y % 4 == 0 && (y % 400 == 0 || y % 100 != 0)
	};

	let year = rtc_data.century as u16 * 100 + rtc_data.year as u16;

	for y in UNIX_YEAR..year {
		time += (if is_leap_year(y) { 366 } else { 365 }) * SECONDS_IN_A_DAY;
	}

	if rtc_data.month > 1 {
		for m in 0..rtc_data.month - 1 {
			time += MONTH_DAYS[m as usize] as u64 * SECONDS_IN_A_DAY;
		}
	}

	if rtc_data.day > 0 {
		time += (rtc_data.day as u64 - 1) * SECONDS_IN_A_DAY;
	}

	if rtc_data.hour > 0 {
		time += (rtc_data.hour - 1) as u64 * 60 * 60;
	}

	time += rtc_data.minute as u64 * 60;
	time += rtc_data.second as u64;
	
	time
}
