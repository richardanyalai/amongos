//! Constants, structures and functions for serial port communication

#![allow(dead_code)]

use core::fmt::{Arguments, Result, Write};

use crate::{
	drivers::tty::print,
	interrupts::{pic::send_eoi, HALT},
	io::ports::{inb, outb},
	utils::logger::kok,
};

use spin::Mutex;

/// Enum of serial port numbers
#[repr(u16)]
#[derive(Clone, Copy)]
enum Port {
	COM1 = 0x3F8,
	COM2 = 0x2F8,
	COM3 = 0x3E8,
	COM4 = 0x2E8,
}

use Port::*;

const SERIAL_IRQ: u8 = crate::interrupts::irq::IRQ_4;

/// Implementation for the serial device struct
///
/// * `device` - serial port number
///
/// For more information please read:
///
/// * <https://wiki.osdev.org/Serial_Port>
/// * <https://wiki.osdev.org/Serial_Ports>
struct SerialDevice(Port);

impl SerialDevice {
	/// Initialize the serial device
	///
	/// # Safety
	///
	/// This function is unsafe because it calls multiple unsafe functions for
	/// hardware communication.
	#[rustfmt::skip]
	pub unsafe fn init(&self) -> u8 {
		let device = self.0 as u16;

		outb(device + 1, 0x00);	// Disable all interrupts
		outb(device + 3, 0x80);	// Enable DLAB (set baud rate divisor)
		outb(device,     0x03);	// Set divisor to 3 (lo byte) 38400 baud
		outb(device + 1, 0x00);	//                  (hi byte)
		outb(device + 3, 0x03);	// 8 bits, no parity, one stop bit
		outb(device + 2, 0xC7);	// Enable FIFO, clear them, with 14-byte
								// threshold
		outb(device + 4, 0x0B);	// IRQs enabled, RTS/DSR set
		outb(device + 4, 0x1E);	// Set in loopback mode, test the serial chip
		outb(device,     0xAE);	// Test serial chip (send byte 0xAE and check
								// if serial returns same byte)

		// Check if serial is faulty (i.e: not same byte as sent)
		if inb(device) != 0xAE {
			return 1;
		}

		// If serial is not faulty set it in normal operation mode
		// (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
		outb(device + 4, 0x0F);

		0
	}

	/// Receive data from serial input
	fn received(&self) -> u8 {
		unsafe { inb(self.0 as u16 + 5) & 1 }
	}

	/// Read from serial input
	pub fn read(&self) -> u8 {
		unsafe {
			while self.received() == 0 {
				HALT!();
			}

			inb(self.0 as u16)
		}
	}

	/// Read asynchronously from serial input
	fn read_async(&self) -> u8 {
		unsafe { inb(self.0 as u16) }
	}

	/// Check whether we can send data on serial output
	fn is_transmit_empty(&self) -> u8 {
		unsafe { inb(self.0 as u16 + 5) & 0x20 }
	}

	/// Send a byte to the serial output
	///
	/// * `out` - byte to be sent to serial output
	pub fn send(&self, out: u8) {
		unsafe {
			while self.is_transmit_empty() == 0 {
				HALT!();
			}

			outb(self.0 as u16, out)
		}
	}

	/// Print to the serial output
	///
	/// * `out` - array of bytes to be sent to serial output
	fn print(&mut self, out: &[u8]) {
		for item in out {
			self.send(*item)
		}
	}
}

impl Write for SerialDevice {
	fn write_str(&mut self, s: &str) -> Result {
		self.print(s.as_bytes());

		Ok(())
	}
}

static SERIAL_DEVICE_A: Mutex<SerialDevice> = Mutex::new(SerialDevice(COM1));
static SERIAL_DEVICE_B: Mutex<SerialDevice> = Mutex::new(SerialDevice(COM2));
static SERIAL_DEVICE_C: Mutex<SerialDevice> = Mutex::new(SerialDevice(COM3));
static SERIAL_DEVICE_D: Mutex<SerialDevice> = Mutex::new(SerialDevice(COM4));

/// IRQ handler for COM1 port
///
/// # Safety
///
/// This function is unsafe because it reads data from and writes data to
/// a hardware device.
unsafe fn com_1_handler() {
	let mut data = SERIAL_DEVICE_A.lock().read();

	send_eoi(SERIAL_IRQ);

	if data == 13 {
		data = b'\n'
	}

	print!("{}", data as char);

	SERIAL_DEVICE_A.lock().send(data);
}

/// IRQ handler for COM2 port
///
/// # Safety
///
/// This function is unsafe because it reads data from and writes data to
/// a hardware device.
unsafe fn com_2_handler() {
	let data = SERIAL_DEVICE_B.lock().read();

	send_eoi(SERIAL_IRQ - 1);

	SERIAL_DEVICE_B.lock().send(data);
}

/// Helper function for printing formatted string
///
/// * `args` - arguments for **core::fmt::Write**
#[doc(hidden)]
pub fn _print(args: Arguments) {
	SERIAL_DEVICE_A.lock().write_fmt(args).unwrap()
}

/// Macro for printing formatted string to serial output
pub macro print_serial {
	($($arg:tt)*) =>
	($crate::drivers::peripheral::serial::_print(format_args!($($arg)*)))
}

/// Macro for printing formatted string ending with linefeed to serial output
pub macro println_serial {
	() => (print_serial!("\n")),
	($($arg:tt)*) => (print_serial!("{}\n", format_args!($($arg)*)))
}

/// Initializing serial ports and registering IRQ handlers
/// 
/// # Safety
/// 
/// This function is unsafe because calls unsafe functions and uses static
/// mutable objects.
#[rustfmt::skip]
pub fn init() {
	unsafe {
		SERIAL_DEVICE_A.lock().init();
		SERIAL_DEVICE_B.lock().init();
	}

	use crate::interrupts::register_interrupt_handler;

	register_interrupt_handler(SERIAL_IRQ,     true, com_1_handler);
	register_interrupt_handler(SERIAL_IRQ - 1, true, com_2_handler);

	unsafe {
		outb(COM1 as u16 + 1, 0x01);
		outb(COM2 as u16 + 1, 0x01);
		outb(COM3 as u16 + 1, 0x01);
		outb(COM4 as u16 + 1, 0x01);
	}

	kok!("Initialized serial port drivers");
}
