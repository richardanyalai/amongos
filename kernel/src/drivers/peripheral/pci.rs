//! Constants, structures and functions for Peripheral Component Interconnect
//!
//! Useful links:
//! https://wiki.osdev.org/PCI
//! https://en.wikipedia.org/wiki/PCI_configuration_space
//!
//! PCI vendor & device lists:
//! https://devicehunt.com
//! https://pci-ids.ucw.cz/read/PC/
//!
//! PCI classes and subclasses can be found here:
//! http://www.lowlevel.eu/wiki/Peripheral_Component_Interconnect
//!
//! I found this repository very useful to understand how to detect PCI
//! devices: https://github.com/levex/osdev/blob/master/drivers/pci/pci.c
//!
//! The video that helped me the most:
//! https://www.youtube.com/watch?v=GE7iO2vlLD4
//!
//! Base Address Registers:
//! https://www.youtube.com/watch?v=yqjDYF4NCXg&t=1932s

use alloc::{format, string::String, vec, vec::Vec};

use crate::{
	drivers::tty::tty_set_fgcolor,
	io::ports::{inl, outl, outw},
	utils::logger::{kok, log},
};

use spin::Mutex;

// Constants

const PCI_CP: u16 = 0xCF8;
const PCI_DP: u16 = 0xCFC;

/// PCI device classes
#[rustfmt::skip]
const CLASSES: &[&str] = &[
	/* 00 */ "Unclassified device",
	/* 01 */ "Mass storage controller",
	/* 02 */ "Network controller",
	/* 03 */ "Display controller",
	/* 04 */ "Multimedia controller",
	/* 05 */ "Memory controller",
	/* 06 */ "Bridge",
	/* 07 */ "Communication controller",
	/* 08 */ "Generic system peripheral",
	/* 09 */ "Input device controller",
	/* 0A */ "Docking station",
	/* 0B */ "Processor",
	/* 0C */ "Serial bus controller",
	/* 0D */ "Wireless controller",
	/* 0E */ "Intelligent controller",
	/* 0F */ "Satellite communications controller",
	/* 10 */ "Encryption controller",
	/* 11 */ "Signal processing controller",
	/* 12 */ "Processing accelerators",
	/* 13 */ "Non-Essential Instrumentation",
	/* 14 */ "",
	/* 15 */ "",
	/* 40 */ "Coprocessor",
	/* 64 */ "",
	/* FF */ "Unassigned class",
];

/// PCI device vendors
#[rustfmt::skip]
const VENDORS: &[&str] = &[
	/* ???? */ "Unknown",
	/* 1002 */ "Advanced Micro Devices, Inc. [AMD/ATI]",
	/* 1022 */ "Advanced Micro Devices, Inc. [AMD]",
	/* 106B */ "Apple, Inc.",
	/* 1234 */ "QEMU",
	/* 1AF4 */ "Red Hat, Inc.",
	/* 1C5C */ "SK hynix",
	/* 80EE */ "VirtualBox",
	/* 8086 */ "Intel Corporation",
];

/// PCI device identifiers
#[rustfmt::skip]
const DEVICES: &[&str] = &[
	/* UNKNOWN */   "UNKNOWN",

	/////////////////////////////////////
	////              AMD            ////
	/////////////////////////////////////

	/* 1002:1636 */ "Renoir",
	/* 1002:1637 */ "Renoir Radeon High Definition Audio Controller",

	/* 1022:2000 */ "am79c973/Am79C975 PCnet-FAST III",
	/* 1022:1448 */ "Renoir Device 24: Function 0",
	/* 1022:1449 */ "Renoir Device 24: Function 1",
	/* 1022:144A */ "Renoir Device 24: Function 2",
	/* 1022:144B */ "Renoir Device 24: Function 3",
	/* 1022:144C */ "Renoir Device 24: Function 4",
	/* 1022:144D */ "Renoir Device 24: Function 5",
	/* 1022:144E */ "Renoir Device 24: Function 6",
	/* 1022:144F */ "Renoir Device 24: Function 7",
	/* 1022:15DF */ "Family 17h (Models 10h-1fh) Platform Security Processor",
	/* 1022:15E2 */ "Renoir Device 24: Function 3",
	/* 1022:15E3 */ "Family 17h (Models 10h-1fh) HD Audio Controller",
	/* 1022:1630 */ "Renoir/Cezanne Root Complex",
	/* 1022:1631 */ "Renoir/Cezanne IOMMU",
	/* 1022:1632 */ "Renoir PCIe Dummy Host Bridge",
	/* 1022:1634 */ "Renoir/Cezanne PCIe GPP Bridge",
	/* 1022:1635 */ "Renoir/Cezanne PCIe GPP Bridge",
	/* 1022:1639 */ "Renoir/Cezanne PCIe GPP Bridge",
	/* 1022:7901 */ "FCH SATA Controller [AHCI mode]",
	/* 1022:790B */ "FCH SMBus Controller",
	/* 1022:790E */ "FCH LPC Bridge",

	/////////////////////////////////////
	////          Apple, Inc.        ////
	/////////////////////////////////////

	/* 106B:003F */ "KeyLargo/Intrepid USB",

	/////////////////////////////////////
	////             QEMU            ////
	/////////////////////////////////////

	/* 1234:1111 */ "QEMU Virtual Video Controller",

	/////////////////////////////////////
	////         Red Hat, Inc.       ////
	/////////////////////////////////////

	/* 1AF4:1002 */ "Virtio memory balloon",
	/* 1AF4:1003 */ "Virtio console",

	/* 1B36:0100 */ "QXL paravirtual graphic card",

	//////////////////////////////////////
	////          VirtualBox         ////	
	/////////////////////////////////////

	/* 80EE:BEEF */ "VirtualBox Graphics Adapter",
	/* 80EE:CAFE */ "VirtualBox Guest Service",

	/////////////////////////////////////
	////          Intel Corp.        ////
	/////////////////////////////////////

	/* 8086:100E */ "82540EM Gigabit Ethernet Controller",
	/* 8086:100F */ "82545EM Gigabit Ethernet Controller",
	/* 8086:1237 */ "440FX - 82441FX PMC [Natoma]",
	/* 8086:2415 */ "82801AA AC'97 Audio Controller",
	/* 8086:2668 */ "82801FB/FBM/FR/FW/FRW (ICH6 Family) High Definition Audio \
					Controller",
	/* 8086:2723 */ "Wi-Fi 6 AX200",
	/* 8086:2934 */ "82801I (ICH9 Family) USB UHCI Controller #1",
	/* 8086:2935 */ "82801I (ICH9 Family) USB UHCI Controller #2",
	/* 8086:2936 */ "82801I (ICH9 Family) USB UHCI Controller #3",
	/* 8086:293A */ "82801I (ICH9 Family) USB2 EHCI Controller #1",
	/* 8086:7000 */ "82371SB PIIX3 ISA [Natoma/Triton II]",
	/* 8086:7010 */ "82371SB PIIX3 IDE [Natoma/Triton II]",
	/* 8086:7111 */ "82371AB/EB/MB PIIX4 IDE",
	/* 8086:7113 */ "82371AB/EB/MB PIIX4 ACPI",
];

#[rustfmt::skip]
macro class_index($class:expr) {
	match $class {
		0x01 =>  1,
		0x02 =>  2,
		0x03 =>  3,
		0x04 =>  4,
		0x05 =>  5,
		0x06 =>  6,
		0x07 =>  7,
		0x08 =>  8,
		0x09 =>  9,
		0x0A => 10,
		0x0B => 11,
		0x0C => 12,
		0x0D => 13,
		0x0E => 14,
		0x0F => 15,
		0x10 => 16,
		0x11 => 17,
		0x12 => 18,
		0x13 => 19,
		0x14 => 20,
		0x15 => 21,
		0x40 => 22,
		0x64 => 23,
		0xFF => 24,
		_    =>  0,
	}
}

#[rustfmt::skip]
macro vendor_index($vendor:expr) {
	match $vendor {
		0x1002 => 1,
		0x1022 => 2,
		0x106B => 3,
		0x1234 => 4,
		0x1AF4 => 5,
		0x1C5C => 6,
		0x80EE => 7,
		0x8086 => 8,
		_      => 0
	}
}

#[rustfmt::skip]
macro device_index($device:expr) {
	match $device {
		0x1636 =>  1,
		0x1637 =>  2,
		0x2000 =>  3,
		0x1448 =>  4,
		0x1449 =>  5,
		0x144A =>  6,
		0x144B =>  7,
		0x144C =>  8,
		0x144D =>  9,
		0x144E => 10,
		0x144F => 11,
		0x15DF => 12,
		0x15E2 => 13,
		0x15E3 => 14,
		0x1630 => 15,
		0x1631 => 16,
		0x1632 => 17,
		0x1634 => 18,
		0x1635 => 19,
		0x1639 => 20,
		0x7901 => 21,
		0x790B => 22,
		0x790E => 23,
		0x003F => 24,
		0x1111 => 25,
		0x1002 => 26,
		0x1003 => 27,
		0x0100 => 28,
		0xBEEF => 29,
		0xCAFE => 30,
		0x100E => 31,
		0x100F => 32,
		0x1237 => 33,
		0x2415 => 34,
		0x2668 => 35,
		0x2723 => 36,
		0x2934 => 37,
		0x2935 => 38,
		0x2936 => 39,
		0x293A => 40,
		0x7000 => 41,
		0x7010 => 42,
		0x7111 => 43,
		0x7113 => 44,
		_      =>  0
	}
}

#[rustfmt::skip]
#[derive(Default)]
struct PciDevice {
	port_base  : u32,
	interrupt  : u32,

	bus        : u16,
	device_num : u16,
	function   : u16,
	vendor_id  : u16,
	device_id  : u16,

	class_id   : u8,
	subclass_id: u8,
	interface  : u8,
	revision   : u8,

	driver     : Option<PciDriver>,
}

#[rustfmt::skip]
#[allow(unused)]
#[derive(Default)]
struct PciDeviceId {
	vendor  : u32,
	device  : u32,
	function: u32,
}

#[rustfmt::skip]
#[allow(unused)]
#[derive(Default)]
struct PciDriver {
	table: PciDeviceId,
	name:  String,
}

#[rustfmt::skip]
#[derive(PartialEq)]
enum BarType {
	Mem = 0,
	IO  = 1,
}

#[rustfmt::skip]
#[allow(unused)]
struct Bar {
	pub prefetchable: bool,
	pub address     : u32,
	pub size        : u32,
	pub typ         : BarType,
}

#[rustfmt::skip]
fn read_word(bus: u16, device: u16, function: u16, offset: u16) -> u16 {
	let address = 0x1_u32         << 31
		| ((bus        as u32 & 0xFF) << 16)
		| ((device     as u32 & 0x1F) << 11)
		| ((function   as u32 & 0x07) <<  8)
		|  (offset     as u32 & 0xFC);

	unsafe {
		outl(PCI_CP, address);
		(inl(PCI_DP) >> (8 * (offset % 4))) as u16
	}
}

#[rustfmt::skip]
fn write_word(bus: u16, device: u16, function: u16, offset: u32, value: u32) {
	let address = 0x1_u32         << 31
		| ((bus        as u32 & 0xFF) << 16)
		| ((device     as u32 & 0x1F) << 11)
		| ((function   as u32 & 0x07) <<  8)
		|  (offset            & 0xFC);

	unsafe {
		outw(PCI_CP, address as u16);
		outw(PCI_DP, value as u16);
	}
}

fn get_bar(bus: u16, device: u16, function: u16, bar: u16) -> Bar {
	let mut res = Bar {
		prefetchable: false,
		address: 0,
		size: 0,
		typ: BarType::IO,
	};

	let header_type = read_word(bus, device, function, 0x0E) & 0x7F;
	let max_bars = 6 - (4 * header_type);

	if bar >= max_bars {
		return res;
	}

	let bar_value = read_word(bus, device, function, 0x10 + 4 * bar);

	res.typ = if bar_value & 0x1 != 0 {
		res.address = (bar_value & !0x3) as u32;

		BarType::IO
	} else {
		match (bar_value >> 1) & 0x3 {
			1 => {} // 20-bit mode
			2 => {} // 64-bit mode
			_ => {} // 32-bit mode
		}

		BarType::Mem
	};

	res
}

static DEVICE_LIST: Mutex<Vec<PciDevice>> = Mutex::new(vec![]);

pub fn init() {
	probe();
	dump();
	list();

	kok!("Initialized PCI device drivers");
}

fn probe() {
	// There can be a total of 256 buses but for the smallest amount of delay
	// I just look at the first 10 buses
	for bus in 0..10 {
		for device in 0..32 {
			for function in 0..8 {
				let vendor = read_word(bus, device, function, 0x00);

				if vendor == 0xffff {
					continue;
				}

				let mut dev = PciDevice::default();

				for bar_num in 0..6 {
					let bar = get_bar(bus, device, function, bar_num);

					if bar.address != 0 && bar.typ == BarType::IO {
						dev.port_base = bar.address;
					}
				}

				dev.bus = bus;
				dev.device_num = device;
				dev.class_id = read_word(bus, device, function, 0x0B) as u8;
				dev.subclass_id = read_word(bus, device, function, 0x0A) as u8;
				dev.vendor_id = vendor;
				dev.device_id = read_word(bus, device, function, 0x02);
				dev.interface = read_word(bus, device, function, 0x09) as u8;
				dev.function = function;
				dev.revision = read_word(bus, device, function, 0x08) as u8;
				dev.interrupt =
					(read_word(bus, device, function, 0x3C) + 0x20) as u32;

				/*
								match dev.vendor_id {
									_ => match dev.device_id {
										_ => {}
									},
								}
				*/

				DEVICE_LIST.lock().push(dev);
			}
		}
	}
}

pub fn list() {
	use crate::graphx::color::Colors::{LightGray, White};

	let device_list = DEVICE_LIST.lock();

	for i in 0..device_list.len() {
		let dev = &device_list[i];

		tty_set_fgcolor!(LightGray);

		log!(
			"{:02x}.{:02x}.{:x} {}, {}, {}",
			dev.bus,
			dev.device_num,
			dev.function,
			CLASSES[class_index!(dev.class_id)],
			VENDORS[vendor_index!(dev.vendor_id)],
			DEVICES[device_index!(dev.device_id)]
		);

		tty_set_fgcolor!(White);
	}
}

pub fn dump() {
	use crate::graphx::color::Colors::{LightGray, White};

	let device_list = DEVICE_LIST.lock();

	for i in 0..device_list.len() {
		let dev = &device_list[i];

		tty_set_fgcolor!(LightGray);

		if let Some(driver) = &dev.driver {
			log!(
				"[{:04x}:{:04x}:{:02x}] => {}",
				dev.vendor_id,
				dev.device_id,
				dev.function,
				driver.name
			);
		} else {
			log!(
				"{:02x}.{:02x}.{:x} {:02x}{:02x}: {:04x}:{:04x}{}",
				dev.bus,
				dev.device_num,
				dev.function,
				dev.class_id,
				dev.subclass_id,
				dev.vendor_id,
				dev.device_id,
				if dev.revision > 0 {
					format!(" (rev {:02x})", dev.revision)
				} else {
					String::new()
				}
			);
		}

		tty_set_fgcolor!(White);
	}
}
