//! Constants, structures and functions for parallel port communication

#![allow(dead_code)]

use core::fmt::{Arguments, Result, Write};

use crate::{
	interrupts::{
		irq::{IRQ_5, IRQ_7},
		register_interrupt_handler,
	},
	io::ports::{inb, io_wait, outb},
	utils::logger::{kinf, kok, log},
};

use spin::Mutex;

const PPORT_A_ADDR: u16 = 0x378;
const PPORT_B_ADDR: u16 = 0x278;
const PPORT_C_ADDR: u16 = 0x3BC;

/// Structure representing a parallel port
/// 
/// A computer can have from 0 up to 4 parallel ports.
/// 
/// * `base_addr` - the base address / data register (in/out)
/// * `status_register` - the status register (in)
/// * `control_register` - the control register (out)
/// * `epp_addr` - Enhanced Parallel Port address (in/out)
/// * `epp_data` - Enhanced Parallel Port address (in/out)
/// 
/// For more details please read:
/// * <https://en.wikipedia.org/wiki/Parallel_port>
/// * <https://wiki.osdev.org/Parallel_port>
/// * <https://www.tau.ac.il/~flaxer/edu/course/processcontrol/slides/g.pdf>
#[rustfmt::skip]
#[derive(Default)]
struct ParallelPort {
	base_addr       : u16,
	status_register : u16,
	control_register: u16,
	epp_addr        : u16,
	epp_data        : u16,
}

impl ParallelPort {
	/// Initialize a parallel port
	#[rustfmt::skip]
	pub fn init(&mut self, base_addr: u16) {
		self.base_addr        = base_addr;
		self.status_register  = base_addr + 1;
		self.control_register = base_addr + 2;

		kinf!("Initialized parallel port {:#X}", self.base_addr);
	}

	/// Write a byte to parallel port
	///
	/// # Safety
	///
	/// This function is unsafe because calls unsafe functions for hardware
	/// communication.
	unsafe fn write_byte(&mut self, data: u8) {
		// Wait for the printer to be receptive.
		while ((!inb(self.status_register)) & 0x80) != 0 {
			io_wait();
		}

		// Now put the data onto the data lines.
		outb(self.base_addr, data);

		// Now pulse the strobe line to tell the printer to read the data.
		let ctrl = inb(self.control_register);

		outb(self.control_register, ctrl | 1);
		io_wait();
		outb(self.control_register, ctrl);

		// Now wait for the printer to finish processing.
		while ((!inb(self.status_register)) & 0x80) != 0 {
			io_wait();
		}
	}

	/// Write a string to parallel port
	///
	/// * `out` - the string to be sent to parallel port
	pub fn write(&mut self, out: &str) {
		for byte in out.as_bytes() {
			if *byte == b'\0' {
				break;
			}

			unsafe {
				self.write_byte(*byte);
			}
		}
	}
}

impl Write for ParallelPort {
	fn write_str(&mut self, s: &str) -> Result {
		self.write(s);

		Ok(())
	}
}

lazy_static! {
	static ref PARALLEL_PORT_A: Mutex<ParallelPort> =
		Mutex::new(ParallelPort::default());
}

/// IRQ handler for parallel ports 1 and 2
unsafe fn handler_1() {
	log!("Interrupt received from parallel port A");
}

/// IRQ handler for parallel port 3
unsafe fn handler_2() {
	log!("Interrupt received from parallel port B");
}

/// Helper function for printing formatted string
///
/// * `args` - arguments for **core::fmt::Write**
#[doc(hidden)]
pub fn _print(args: Arguments) {
	PARALLEL_PORT_A.lock().write_fmt(args).unwrap()
}

/// Macro for printing formatted string to parallel output
pub macro print_parallel {
	($($arg:tt)*) =>
	($crate::drivers::peripheral::parallel::_print(format_args!($($arg)*)))
}

/// Macro for printing formatted string ending with linefeed to parallel output
pub macro println_parallel {
	() => (print_parallel!("\n")),
	($($arg:tt)*) => (print_parallel!("{}\n\r", format_args!($($arg)*)))
}

/// Initialize parallel ports and set up IRQ handlers
pub fn init() {
	PARALLEL_PORT_A.lock().init(PPORT_A_ADDR);

	register_interrupt_handler(IRQ_5, true, handler_2);
	register_interrupt_handler(IRQ_7, true, handler_1);

	kok!("Initialized parallel port drivers");
}
