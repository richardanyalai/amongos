//! Structs and functions for ACPI
//!
//! Original post:
//! https://forum.osdev.org/viewtopic.php?t=16990
//!
//! Useful links:
//! https://wiki.osdev.org/ACPI
//! https://wiki.osdev.org/Shutdown

use core::{mem::size_of, ptr::null, slice::from_raw_parts};

use crate::{
	bootboot::{Bootboot, BOOTBOOT_INFO},
	interrupts::pit::sleep,
	io::ports::*,
	utils::logger::{kerr, kok},
};

use spin::Mutex;

#[derive(Default)]
#[rustfmt::skip]
struct Acpi {
	acpi_enable : u8,
	acpi_disable: u8,
	pm1_cnt_len : u8,
	slp_typ_a   : u16,
	slp_typ_b   : u16,
	slp_en      : u16,
	sci_en      : u16,
	smi_cmd     : u32,
	pm1_a_cnt   : u32,
	pm1_b_cnt   : u32,
}

#[repr(C, packed)]
#[rustfmt::skip]
struct RSDPtr {
	signature: [u8; 8],
	checksum : u8,
	oem_id   : [u8; 6],
	revision : u8,
	rsdt_addr: u32,
}

#[repr(C, packed)]
#[allow(clippy::upper_case_acronyms)]
#[rustfmt::skip]
struct FACP {
	signature   : [u8; 4],
	length      : u32,
	unneeded1   : [u8; 32],
	dsdt        : u32,
	unneeded2   : [u8; 4],
	smi_cmd     : u32,
	acpi_enable : u8,
	acpi_disable: u8,
	unneeded3   : [u8; 10],
	pm1a_cnt_blk: u32,
	pm1b_cnt_blk: u32,
	unneeded4   : [u8; 17],
	pm1_cnt_len : u8,
}

lazy_static! {
	static ref ACPI: Mutex<Acpi> = Mutex::new(Acpi::default());
}

/// Checks whether RSD pointer is valid
///
/// If valid, returns the pointer, otherwise it returns a null pointer
fn check_rsd_ptr(ptr: *const u32) -> *const u32 {
	let rsdp = ptr as *const RSDPtr;
	let bptr: *const u8;
	let mut check = 0_u8;

	if unsafe { from_raw_parts(ptr as *const u8, 8) } == "RSD PTR ".as_bytes()
	{
		// check checksum rsdpd
		bptr = ptr as *const u8;

		for i in 0..size_of::<RSDPtr>() {
			let val = unsafe { *(bptr.add(i)) };

			if check as u16 + val as u16 <= 255 {
				check += val;
			} else {
				check = ((check as u16 + val as u16) % 256) as u8;
			}
		}

		if check == 0 {
			return unsafe { (*rsdp).rsdt_addr as *const u32 };
		}
	}

	null()
}

/// Returns the RSD pointer
fn get_rsd_ptr() -> *const u32 {
	let mut addr = 0x000E0000 as *const u32;
	let mut rsdp: *const u32;

	// search below the 1mb mark for RSDP signature
	while (addr as u32) < 0x00100000 {
		rsdp = check_rsd_ptr(addr);

		if !rsdp.is_null() {
			return rsdp;
		}

		unsafe {
			addr = addr.add(0x10 / size_of::<*const u32>());
		}
	}

	// at address 0x40:0x0E is the RM segment of the ebda
	let ebda = (unsafe { *(0x40E as *const u16) } as u32 * 0x10) & 0x000FFFFF;

	addr = ebda as *const u32;

	// search Extended BIOS Data Area for the Root System Description
	// Pointer signature
	while (addr as u32) < ebda + 1024 {
		rsdp = check_rsd_ptr(addr);

		if !rsdp.is_null() {
			return rsdp;
		}

		addr = unsafe { addr.add(0x10 / size_of::<*const u32>()) };
	}

	null()
}

/// Checks whether the header matches the signature
///
/// Returns 0 on match, -1 otherwise
fn check_header(ptr: *const u32, sig: &str) -> i32 {
	if unsafe { from_raw_parts(ptr as *const u8, 4) } == sig.as_bytes() {
		let check_ptr = ptr as *const u8;
		let len = unsafe { *(ptr.add(1)) };
		let mut check = 0_u8;

		for i in 0..len {
			let val = unsafe { *(check_ptr.add(i as usize)) };

			if check as u16 + val as u16 <= 255 {
				check += val;
			} else {
				check = ((check as u16 + val as u16) % 256) as u8;
			}
		}

		if check == 0 {
			return 0;
		}
	}

	-1
}

/// Enables ACPI
///
/// Returns 0 on success, -1 on failure
pub fn enable() -> i32 {
	let acpi = ACPI.lock();

	// check whether acpi is enabled
	if (unsafe { inw(acpi.pm1_a_cnt as u16) } & acpi.sci_en) == 0 {
		// check whether acpi can be enabled
		if acpi.smi_cmd != 0 && acpi.acpi_enable != 0 {
			// send acpi enable command
			unsafe {
				outb(acpi.smi_cmd as u16, acpi.acpi_enable);
			}

			// give 3 seconds time to enable acpi

			let mut i = 0_u16;

			while i < 300 {
				i += 1;

				if (unsafe { inw(acpi.pm1_a_cnt as u16) } & acpi.sci_en) == 1 {
					break;
				}

				sleep!(10);
			}

			if acpi.pm1_b_cnt != 0 {
				while i < 300 {
					i += 1;

					if (unsafe { inw(acpi.pm1_b_cnt as u16) } & acpi.sci_en)
						== 1
					{
						break;
					}

					sleep!(10);
				}
			}

			if i < 300 {
				kok!("Enabled ACPI");

				return 0;
			} else {
				kerr!("Couldn't enable ACPI");

				return -1;
			}
		} else {
			kerr!("No known way to enable ACPI");

			return -1;
		}
	}

	// already enabled
	0
}

/// Initializes ACPI
///
/// Returns 0 on success, -1 on failure
pub fn init() -> i32 {
	let ptr =
		unsafe { (*(BOOTBOOT_INFO as *const Bootboot)).arch.x86_64.acpi_ptr }
			as *const u32;

	let mut ptr = if !check_rsd_ptr(ptr).is_null() {
		ptr
	} else {
		get_rsd_ptr()
	};

	let mut acpi = ACPI.lock();

	unsafe {
		// check if address is correct  ( if acpi is available on this pc )
		if !ptr.is_null() && check_header(ptr, "RSDT") == 0 {
			// the RSDT contains an unknown number of pointers to acpi tables
			let mut entries = ((*(ptr.add(1)) - 36) / 4) as i16;

			// skip header information
			ptr = ptr.add(9);

			while 0 < entries {
				entries -= 1;

				// check whether the desired table is reached
				if check_header(*ptr as *const u32, "FACP") == 0 {
					entries = -2;
					let facp = *ptr as *const FACP;
					let dsdt = (*facp).dsdt as *const u32;

					if check_header(dsdt, "DSDT") == 0 {
						// search the \_S5 package in the DSDT
						// skip header
						let mut s5_addr = dsdt.add(36) as *const u8;
						let mut dsdt_length = *(dsdt.add(1)) - 36;

						while 0 < dsdt_length {
							dsdt_length -= 1;

							if from_raw_parts(s5_addr, 4) == "_S5_".as_bytes()
							{
								break;
							}

							s5_addr = s5_addr.add(1);
						}

						// check if \_S5 was found
						if dsdt_length > 0 {
							// check for valid AML structure
							#[rustfmt::skip]
							if (*(s5_addr.sub(1)) == 0x08
								|| (
									*(s5_addr.sub(2)) == 0x08 &&
									*(s5_addr.sub(1)) == b'\\'
								)
							) && *(s5_addr.add(4))
								== 0x12
							{
								s5_addr = s5_addr.add(5);

								// calculate Pkglength size
								let offset = ((*s5_addr & 0xC0) >> 6) + 2;
								s5_addr = s5_addr.add(offset as usize);

								if *s5_addr == 0x0A {
									// skip prefix
									s5_addr = s5_addr.add(1);
								}

								acpi.slp_typ_a = (*s5_addr as u16) << 10;

								s5_addr = s5_addr.add(1);

								if *s5_addr == 0x0A {
									// skip prefix
									s5_addr = s5_addr.add(1);
								}

								acpi.slp_typ_b    = (*s5_addr as u16) << 10;

								acpi.smi_cmd      = (*facp).smi_cmd;

								acpi.acpi_enable  = (*facp).acpi_enable;
								acpi.acpi_disable = (*facp).acpi_disable;

								acpi.pm1_a_cnt    = (*facp).pm1a_cnt_blk;
								acpi.pm1_b_cnt    = (*facp).pm1b_cnt_blk;

								acpi.pm1_cnt_len  = (*facp).pm1_cnt_len;

								acpi.slp_en       = 1 << 13;
								acpi.sci_en       = 1;

								kok!("Initialized ACPI");

								return 0;
							} else {
								kerr!("\\_S5 parse error");
							}
						} else {
							kerr!("\\_S5 not present");
						}
					} else {
						kerr!("DSDT invalid");
					}
				}

				ptr = ptr.add(1);
			}

			kerr!("No valid FACP present");
		} else {
			kerr!("No ACPI");
		}
	}

	-1
}

/// Shut down the machine
pub fn shut_down() {
	let acpi = ACPI.lock();

	// SCI_EN is set to 1 if acpi shutdown is possible
	if acpi.sci_en == 0 {
		return;
	}

	// send the shutdown command
	unsafe {
		outw(acpi.pm1_a_cnt as u16, acpi.slp_typ_a | acpi.slp_en);
	}

	if acpi.pm1_b_cnt != 0 {
		unsafe {
			outw(acpi.pm1_b_cnt as u16, acpi.slp_typ_b | acpi.slp_en);
		}
	}

	kerr!("ACPI: poweroff failed");
}
