//! Functions for PS/2 keyboard driver

mod keycodes;
mod layout;

use crate::{
	drivers::ps2::*,
	interrupts::{irq::IRQ_1, register_interrupt_handler},
};

use spin::Mutex;

use keycodes::KeyCode::*;
use layout::KEYS;

/// Structure representing a keyboard key
/// 
/// * `key_code` - key code
/// * `char_code` - character code
/// * `shift_char_code` - Shift+<key> character code
/// * `alt_char_code` - Alt+<key> character code
/// * `alt_shift_char_code` - Alt+Shift+<key> character code
/// * `pressed` - whether the key was pressed or released
#[rustfmt::skip]
#[derive(Default)]
pub struct Key {
	pub key_code           : u8,
	pub char_code          : u8,
	pub shift_char_code    : u8,
	pub alt_char_code      : u8,
	pub alt_shift_char_code: u8,
	pub pressed            : bool,
}

#[rustfmt::skip]
impl Key {
	pub fn new(
		key_code           : u8,
		char_code          : u8,
		shift_char_code    : u8,
		alt_char_code      : u8,
		alt_shift_char_code: u8
	) -> Self {
		Self {
			key_code,
			char_code,
			shift_char_code,
			alt_char_code,
			alt_shift_char_code,
			pressed: false,
		}
	}
}

/// Struct for storing modifier key status
/// 
/// * `meta`  - whether the *Super* key is being held down
/// * `ctrl`  - whether the *Control* key is being held down
/// * `alt`   - whether the *Alt* key is being held down
/// * `shift` - whether the *Shift* key is being held down
/// * `numlk` - whether the *Num lock* key is being held down
#[rustfmt::skip]
#[derive(Default)]
pub struct ModKeys {
	meta : bool,
	ctrl : bool,
	alt  : bool,
	shift: bool,
}

/// Struct for storing active keys
/// 
/// * `modifiers`   - a struct representing the modifier keys
/// * `active_keys` - a struct representing the active key
/// * `caps_lock`   - whether CapsLock is active
/// * `num_lock`    - whether NumLock is active
#[rustfmt::skip]
#[derive(Default)]
pub struct ActiveKeys {
	pub modifiers : ModKeys,
	pub active_key: Key,
	pub caps_lock : bool,
	pub num_lock  : bool,
}

lazy_static! {
	static ref ACTIVE_KEYS: Mutex<ActiveKeys> =
		Mutex::new(ActiveKeys::default());
}

static HANDLER: Mutex<fn(active_keys: &ActiveKeys)> =
	Mutex::new(|_active_keys: &ActiveKeys| {});
static STATE: Mutex<(bool, bool)> = Mutex::new((false, false));

/// Initialize keyboard driver
pub fn init() -> PS2Result {
	register_interrupt_handler(IRQ_1, true, process_byte);

	write_device(0, 0xF0)?;
	expect_ack()?;
	write_device(0, 0x00)?;
	expect_ack()?;

	let scancode_set = read_data_port()?;

	if scancode_set != 0x02 {
		kerr!("Wrong scancode set 0x{:x}", scancode_set);
	}

	write_device(0, EnableScan as u8)?;
	expect_ack()?;

	Ok(())
}

/// Processes keyboard scancode
fn process_byte() {
	let byte = unsafe { inb(PORT_DATA) };

	if byte == Ack as u8 {
		return;
	}

	let mut state = STATE.lock();

	match byte {
		0xE0 => state.0 = true,
		0xF0 => state.1 = true,
		_ => {
			let released = state.1;
			let keycode = if !state.0 {
				#[rustfmt::skip]
				match byte {
					0x1c => KEY_A,
					0x32 => KEY_B,
					0x21 => KEY_C,
					0x23 => KEY_D,
					0x24 => KEY_E,
					0x2b => KEY_F,
					0x34 => KEY_G,
					0x33 => KEY_H,
					0x43 => KEY_I,
					0x3b => KEY_J,
					0x42 => KEY_K,
					0x4b => KEY_L,
					0x3a => KEY_M,
					0x31 => KEY_N,
					0x44 => KEY_O,
					0x4d => KEY_P,
					0x15 => KEY_Q,
					0x2d => KEY_R,
					0x1b => KEY_S,
					0x2c => KEY_T,
					0x3c => KEY_U,
					0x2a => KEY_V,
					0x1d => KEY_W,
					0x22 => KEY_X,
					0x35 => KEY_Y,
					0x1a => KEY_Z,
					0x45 => KEY_0,
					0x16 => KEY_1,
					0x1e => KEY_2,
					0x26 => KEY_3,
					0x25 => KEY_4,
					0x2e => KEY_5,
					0x36 => KEY_6,
					0x3d => KEY_7,
					0x3e => KEY_8,
					0x46 => KEY_9,
					0xe  => KEY_GRAVE,
					0x4e => KEY_MINUS,
					0x55 => KEY_EQUAL,
					0x5d => KEY_BACKSLASH,
					0x66 => KEY_BACKSPACE,
					0x29 => KEY_SPACE,
					0xd  => KEY_TAB,
					0x58 => KEY_CAPS_LOCK,
					0x12 => KEY_LEFT_SHIFT,
					0x14 => KEY_LEFT_CTRL,
					0x11 => KEY_LEFT_ALT,
					0x59 => KEY_RIGHT_SHIFT,
					0x5a => KEY_ENTER,
					0x76 => KEY_ESC,
					0x5  => KEY_F1,
					0x6  => KEY_F2,
					0x4  => KEY_F3,
					0xc  => KEY_F4,
					0x3  => KEY_F5,
					0xb  => KEY_F6,
					0x83 => KEY_F7,
					0xa  => KEY_F8,
					0x1  => KEY_F9,
					0x9  => KEY_F10,
					0x78 => KEY_F11,
					0x7  => KEY_F12,
					0x7e => KEY_SCROLL_LOCK,
					0x54 => KEY_LEFT_BRACE,
					0x77 => KEY_NUM_LOCK,
					0x7c => KEY_KP_ASTERISK,
					0x7b => KEY_KP_MINUS,
					0x79 => KEY_KP_PLUS,
					0x71 => KEY_KP_DOT,
					0x70 => KEY_KP_0,
					0x69 => KEY_KP_1,
					0x72 => KEY_KP_2,
					0x7a => KEY_KP_3,
					0x6b => KEY_KP_4,
					0x73 => KEY_KP_5,
					0x74 => KEY_KP_6,
					0x6c => KEY_KP_7,
					0x75 => KEY_KP_8,
					0x7d => KEY_KP_9,
					0x5b => KEY_RIGHT_BRACE,
					0x4c => KEY_SEMICOLON,
					0x52 => KEY_APOSTROPHE,
					0x41 => KEY_COMMA,
					0x49 => KEY_DOT,
					0x4a => KEY_SLASH,
					0x61 => KEY_BACKSLASH,
					_    => KEY_RESERVED,
				}
			} else {
				#[rustfmt::skip]
				match byte {
					0x1f => KEY_LEFT_META,
					0x14 => KEY_RIGHT_CTRL,
					0x27 => KEY_RIGHT_META,
					0x11 => KEY_RIGHT_ALT,
					0x2f => KEY_COMPOSE,
					0x70 => KEY_INSERT,
					0x6c => KEY_HOME,
					0x7d => KEY_PAGE_UP,
					0x71 => KEY_DELETE,
					0x69 => KEY_END,
					0x7a => KEY_PAGE_DOWN,
					0x75 => KEY_UP,
					0x6b => KEY_LEFT,
					0x72 => KEY_DOWN,
					0x74 => KEY_RIGHT,
					0x4a => KEY_KP_SLASH,
					0x5a => KEY_KP_ENTER,
					_    => KEY_RESERVED,
				}
			};

			state.0 = false;
			state.1 = false;

			let mut active_keys = ACTIVE_KEYS.lock();

			match keycode {
				KEY_CAPS_LOCK if released => {
					active_keys.caps_lock = !active_keys.caps_lock;
				}
				KEY_NUM_LOCK if released => {
					active_keys.num_lock = !active_keys.num_lock;
				}
				KEY_LEFT_SHIFT | KEY_RIGHT_SHIFT => {
					active_keys.modifiers.shift = !released;
				}
				KEY_LEFT_ALT | KEY_RIGHT_ALT => {
					active_keys.modifiers.alt = !released;
				}
				KEY_LEFT_CTRL | KEY_RIGHT_CTRL => {
					active_keys.modifiers.ctrl = !released;
				}
				KEY_LEFT_META | KEY_RIGHT_META => {
					active_keys.modifiers.meta = !released;
				}
				KEY_KP_0 | KEY_KP_1 | KEY_KP_2 | KEY_KP_3 | KEY_KP_4
				| KEY_KP_5 | KEY_KP_6 | KEY_KP_7 | KEY_KP_8 | KEY_KP_9
					if !active_keys.num_lock =>
				{
					return
				}
				_ => (),
			}

			if keycode as u8 != 0 {
				active_keys.active_key.key_code = keycode as u8;
				active_keys.active_key.char_code =
					if active_keys.modifiers.shift || active_keys.caps_lock {
						KEYS[keycode as usize].shift_char_code
					} else {
						KEYS[keycode as usize].char_code
					};
				active_keys.active_key.pressed = !released;

				(*HANDLER.lock())(&active_keys);
			}
		}
	}
}

/// Registers a new keyboard handler
///
/// * `handler` - keyboard handler
pub fn register_handler(handler: fn(active_keys: &ActiveKeys)) {
	*HANDLER.lock() = handler;
}
