//! Constants, structures and functions for PS/2 mouse driver
//!
//! Useful links:
//! https://wiki.osdev.org/Mouse_Input
//! https://wiki.osdev.org/PS/2_Mouse

use crate::{
	drivers::ps2::*,
	graphx::{
		cursor_mouse, draw_fillrect, get_screen_height, get_screen_width,
	},
	interrupts::{irq::IRQ_12, register_interrupt_handler},
	utils::logger::{kinf, log},
};

use spin::Mutex;

static BUFFER: Mutex<[u8; 4]> = Mutex::new([0; 4]);
static OFFSET: Mutex<u8> = Mutex::new(0);
static BYTES_PER_PACKET: Mutex<u8> = Mutex::new(3);

/// Mouse event constants
#[rustfmt::skip]
enum MouseEvent {
	/// Whether or not the left mouse button is pressed.
	LeftButton   = 1 << 0,

	/// Whether or not the right mouse button is pressed.
	RightButton  = 1 << 1,

	/// Whether or not the middle mouse button is pressed.
	MiddleButton = 1 << 2,

	/// Whether or not the packet is valid or not.
	AlwaysOne    = 1 << 3,

	/// Whether or not the x delta is negative.
	XSign        = 1 << 4,

	/// Whether or not the y delta is negative.
	YSign        = 1 << 5,

	/// Whether or not the x delta overflowed.
	XOverflow    = 1 << 6,

	/// Whether or not the y delta overflowed.
	YOverflow    = 1 << 7,
}

use MouseEvent::*;

/// Mouse command constants
#[rustfmt::skip]
#[allow(unused)]
#[repr(u8)]
enum Command {
	Reset                  = 0xFF,
	Resend                 = 0xFE,
	SetDefaults            = 0xF6,
	DisablePacketStreaming = 0xF5,
	EnablePacketStreaming  = 0xF4,
	SetSampleRate          = 0xF3,
	GetMouseID             = 0xF2,
	RequestSinglePacket    = 0xEB,
	StatusRequest          = 0xE9,
	SetResolution          = 0xE8,
	DisableScaling         = 0xE6,
	EnableScaling          = 0xE7,
}

use Command::*;

lazy_static! {
	static ref MOUSE_POS: Mutex<(i16, i16)> = Mutex::new((
		(get_screen_width() / 2) as i16,
		(get_screen_height() / 2) as i16
	));
}

/// Initialize mouse driver and register IRQ handler
pub fn init() -> PS2Result {
	register_interrupt_handler(IRQ_12, true, handle);

	enable_scrolling()?;
	enable_five_buttons()?;

	set_sample_rate(80)?;
	set_resolution(0)?;
	set_scaling(false)?;

	write_device(1, EnableScan as u8)?;
	expect_ack()?;

	Ok(())
}

/// Mouse packet processor
fn process_packet() {
	let buf = BUFFER.lock();
	let mut pos = MOUSE_POS.lock();

	let flags = buf[0];

	if *BYTES_PER_PACKET.lock() == 4
		&& ((buf[3] & XOverflow as u8) != 0 || (buf[3] & YOverflow as u8) != 0)
	{
		return;
	}

	if (flags & XOverflow as u8) != 0 || (flags & YOverflow as u8) != 0 {
		return;
	}

	cursor_mouse::clear_old(pos.0 as u16, pos.1 as u16);

	let x_neg = (flags & XSign as u8) != 0;
	let y_neg = (flags & YSign as u8) != 0;

	let dx = buf[1] as i16 + if x_neg { -256 } else { 0 };
	let dy = -(buf[2] as i16 + if y_neg { -256 } else { 0 });

	let width = get_screen_width() as i16;
	let height = get_screen_height() as i16;

	if pos.1 + dy <= 0 {
		pos.1 = 1;
	} else if pos.1 + dy >= height {
		pos.1 = height - 1;
	} else {
		pos.1 += dy;
	}

	if pos.0 + dx <= 0 {
		pos.0 = 1;
	} else if pos.0 + dx >= width {
		pos.0 = width - 1;
	} else {
		pos.0 += dx;
	}

	cursor_mouse::draw_cursor(pos.0 as u16, pos.1 as u16);

	if buf[0] & LeftButton as u8 != 0 && pos.1 + 5 < height {
		draw_fillrect(pos.0 as u16, pos.1 as u16, 5, 5, 0x00ffff);
	}

	if buf[0] & MiddleButton as u8 != 0 {
		log!("Middle button click");
	}

	if buf[0] & RightButton as u8 != 0 {
		log!("Right button click");
	}
}

/// Mouse packet handler
fn handle() {
	let mut offset = OFFSET.lock();
	let byte = read_data_port().unwrap();

	if *offset == 0 && (byte & AlwaysOne as u8) != AlwaysOne as u8 {
		return;
	}

	(*BUFFER.lock())[*offset as usize] = byte;

	*offset = (*offset + 1) % *BYTES_PER_PACKET.lock();

	if *offset == 0 {
		process_packet();
	}
}

/// Sends PS/2 mouse command
fn send_command(command: Command) -> PS2Result {
	write_device(1, command as u8)?;
	expect_ack()?;

	Ok(())
}

/// Sets sample rate
///
/// - `rate` - sample rate
///
/// The rate bust be one of these numbers:
/// 10, 20, 40, 60, 80, 100, 200
fn set_sample_rate(rate: u8) -> PS2Result {
	send_command(SetSampleRate)?;
	write_device(1, rate)?;
	expect_ack()?;

	Ok(())
}

/// Sets resolution
///
/// - `res` - the resolution
///
/// | value|  resolution  |
/// |------|--------------|
/// | 0x00 | 1 count / mm |
/// | 0x01 | 2 count / mm |
/// | 0x02 | 4 count / mm |
/// | 0x03 | 8 count / mm |
fn set_resolution(res: u8) -> PS2Result {
	send_command(SetResolution)?;
	write_device(1, res)?;
	expect_ack()?;

	Ok(())
}

/// Enables scrolling by wheel
fn enable_scrolling() -> PS2Result {
	set_sample_rate(200)?;
	set_sample_rate(100)?;
	set_sample_rate(80)?;

	if identify_device(1)? == MouseScrollWheel {
		*BYTES_PER_PACKET.lock() = 4;

		kinf!("Enabled scrolling wheel");
	} else {
		return Err("Couldn't enable scrolling wheel");
	}

	Ok(())
}

/// Enables five buttons mode
fn enable_five_buttons() -> PS2Result {
	if *BYTES_PER_PACKET.lock() != 4 {
		return Err("Couldn't enable five buttons mode");
	}

	set_sample_rate(200)?;
	set_sample_rate(200)?;
	set_sample_rate(80)?;

	if identify_device(1)? == MouseFiveButtons {
		kinf!("Enabled five buttons mode");
	} else {
		return Err("Less then 5 buttons are available");
	}

	Ok(())
}

/// Sets scaling when enabled is true
fn set_scaling(enable: bool) -> PS2Result {
	send_command(if enable {
		EnableScaling
	} else {
		DisableScaling
	})
}
