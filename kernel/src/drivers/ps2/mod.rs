//! Constants and fuctions for PS/2 devices
//!
//! https://wiki.osdev.org/PS/2
//! https://wiki.osdev.org/%228042%22_PS/2_Controller

pub mod keyboard;
pub mod mouse;

use core::arch::asm;

use crate::{
	io::ports::{inb, outb},
	utils::logger::{kerr, kinf, kok},
};

use spin::Mutex;

// Ports
const PORT_COMM: u16 = 0x64;
const PORT_DATA: u16 = 0x60;

#[rustfmt::skip]
#[derive(PartialEq)]
pub enum DeviceType {
	Mouse            = 0x00,
	MouseScrollWheel = 0x03,
	MouseFiveButtons = 0x04,
	Keyboard,
	KeyboardTranslated,
	Unknown,
}

use DeviceType::*;

// Configuration byte
#[rustfmt::skip]
enum ConfigurationByte {
	FirstPort   = 1,
	SecondPort  = 1 << 1,
	SystemFlag  = 1 << 2,
	FirstClock  = 1 << 4,
	SecondClock = 1 << 5,
	Translation = 1 << 6,
	MustBeZero  = 1 << 7,
}

use ConfigurationByte::*;

// Controller commands
#[rustfmt::skip]
enum ControllerCommand {
	DisableFirst  = 0xAD,
	DisableSecond = 0xA7,
	EnableFirst   = 0xAE,
	EnableSecond  = 0xA8,
	ReadConfig    = 0x20,
	WriteConfig   = 0x60,
	SelfTest      = 0xAA,
	TestFirst     = 0xAB,
	TestSecond    = 0xA9,
	WriteSecond   = 0xD4,
}

use ControllerCommand::*;

// Controller responses
#[rustfmt::skip]
enum ControllerResponse {
	SelfTestOk = 0x55,
	TestOk     = 0x00,
}

use ControllerResponse::*;

// Device commands
#[rustfmt::skip]
enum DeviceCommand {
	Reset       = 0xFF,
	Identify    = 0xF2,
	EnableScan  = 0xF4,
	DisableScan = 0xF5,
}

use DeviceCommand::*;

// Device responses
#[rustfmt::skip]
enum DeviceResponse {
	Ack      = 0xFA,
	ResetAck = 0xAA,
}

use DeviceResponse::*;

type PS2Result = Result<(), &'static str>;
type PS2Data = Result<u8, &'static str>;
type DeviceResult = Result<DeviceType, &'static str>;

static CONTROLLERS: Mutex<[bool; 2]> = Mutex::new([true, true]);

pub fn init() -> PS2Result {
	let mut controllers = *CONTROLLERS.lock();

	// Disable both PS/2 device ports
	// Even if only one is present, disabling the second is harmless
	write_command_port(DisableFirst as u8)?;
	write_command_port(DisableSecond as u8)?;

	unsafe { inb(PORT_DATA) };

	write_command_port(ReadConfig as u8)?;
	let mut config = read_data_port()? | SystemFlag as u8;

	if (config & MustBeZero as u8) == 1 {
		kerr!("Invalid bit set in configuration byte");
	}

	config &= !(FirstPort as u8 | SecondPort as u8 | Translation as u8);

	write_command_port(WriteConfig as u8)?;
	write_data_port(config)?;

	write_command_port(SelfTest as u8)?;

	let mut dual_channels = true;

	if read_data_port()? != SelfTestOk as u8 {
		controllers[0] = false;
		controllers[1] = false;

		return Err("PS/2 controller failed self-test");
	}

	write_command_port(WriteConfig as u8)?;
	write_data_port(config)?;

	write_command_port(EnableSecond as u8)?;
	write_command_port(ReadConfig as u8)?;

	config = read_data_port()?;

	if (config & SecondClock as u8) != 0 {
		kinf!("Only one PS/2 controller is available");
		controllers[1] = false;
		dual_channels = false;
	} else {
		write_command_port(DisableSecond as u8)?;
	}

	write_command_port(TestFirst as u8)?;

	if read_data_port()? != TestOk as u8 {
		kerr!("Failed testing first PS/2 port");
		controllers[0] = false;
	}

	if dual_channels {
		write_command_port(TestSecond as u8)?;

		if read_data_port()? != TestOk as u8 {
			kerr!("Failed testing second PS/2 port");
			controllers[1] = false;
		}
	}

	for (i, controller) in controllers.iter_mut().enumerate() {
		if !*controller {
			continue;
		}

		enable_port(i, true)?;

		write_device(i, Reset as u8)?;
		expect_ack()?;

		let ret = read_data_port()?;

		if i == 0 && ret != ResetAck as u8 {
			kerr!("Keyboard failed to acknowledge reset, sent 0x{:x}", ret);
			enable_port(i, false)?;
			*controller = false;
		} else if i == 1 {
			let ret2 = read_data_port()?;

			if !((ret == ResetAck as u8
				&& (ret2 == Ack as u8 || ret2 == 0x00))
				|| ((ret == Ack as u8 || ret == 0x00)
					&& ret2 == ResetAck as u8))
			{
				kerr!(
					"Mouse failed to acknowledge reset, sent 0x{:x}, 0x{:x}",
					ret,
					ret2
				);
				enable_port(i, false)?;
				*controller = false;
			}
		}

		if i == 0 {
			read_data_port()?;
			enable_port(i, false)?;
		}
	}

	if controllers[0] {
		enable_port(0, true)?;
	}

	for (i, controller) in controllers.iter().enumerate() {
		if *controller {
			let typ = identify_device(i)?;
			let mut driver_called = true;

			match typ {
				Keyboard | KeyboardTranslated => {
					kinf!("Found PS/2 keyboard");

					if let Err(msg) = keyboard::init() {
						kerr!("Couldn't initialize keyboard driver: {}", msg);
					} else {
						kok!("Initialized keyboard driver");
					}
				}
				Mouse | MouseScrollWheel | MouseFiveButtons => {
					kinf!("Found PS/2 mouse");

					if let Err(msg) = mouse::init() {
						kerr!("Couldn't initialize mouse driver: {}", msg);
					} else {
						kok!("Initialized mouse driver");
					}
				}
				_ => driver_called = false,
			}

			if driver_called {
				config |= if i == 0 {
					FirstPort as u8
				} else {
					SecondPort as u8
				};
				config &= !(if i == 0 {
					FirstClock as u8
				} else {
					SecondClock as u8
				});
			}
		}
	}

	write_command_port(WriteConfig as u8)?;
	write_data_port(config)?;

	Ok(())
}

pub fn expect_ack() -> PS2Result {
	if read_data_port()? != Ack as u8 {
		return Err("Device failed to acknowledge command");
	}

	Ok(())
}

fn enable_port(num: usize, enable: bool) -> PS2Result {
	let controllers = *CONTROLLERS.lock();

	if !controllers[num] {
		return Ok(());
	}

	write_command_port(if enable {
		if num == 0 {
			DisableFirst
		} else {
			DisableSecond
		}
	} else if num == 0 {
		EnableFirst
	} else {
		EnableSecond
	} as u8)?;

	read_data_port()?;

	Ok(())
}

pub fn identify_device(num: usize) -> DeviceResult {
	write_device(num, DisableScan as u8)?;
	expect_ack()?;
	write_device(num, Identify as u8)?;
	expect_ack()?;

	let (first, second) = unsafe { (inb(0x60), inb(0x60)) };

	match first {
		0x00 => Ok(Mouse),
		0x03 => Ok(MouseScrollWheel),
		0x04 => Ok(MouseFiveButtons),
		0xAB => match second {
			0x41 | 0xC1 => Ok(KeyboardTranslated),
			0x83 => Ok(Keyboard),
			_ => Ok(Unknown),
		},
		_ => Ok(Unknown),
	}
}

pub fn write_device(num: usize, val: u8) -> PS2Result {
	if num != 0 {
		write_command_port(WriteSecond as u8)?;
	}

	write_data_port(val)?;

	Ok(())
}

/// Waits before reading
pub fn wait_read() -> PS2Result {
	for _ in 0..100_000 {
		if (unsafe { inb(PORT_COMM) } & 0b01) != 0 {
			return Ok(());
		}

		unsafe {
			asm!("pause");
		}
	}

	// Err("PS/2 read timeout")
	Ok(())
}

/// Waits before writing
pub fn wait_write() -> PS2Result {
	for _ in 0..100_000 {
		if (unsafe { inb(PORT_COMM) } & 0b10) == 0 {
			return Ok(());
		}

		unsafe {
			asm!("pause");
		}
	}

	Err("PS/2 write timeout")
}

/// Writes byte to PS/2 command port
///
/// - `val` - the byte that gets written to the command port
pub fn write_command_port(val: u8) -> PS2Result {
	wait_write()?;

	unsafe {
		outb(PORT_COMM, val);
	}

	Ok(())
}

/// Writes byte to PS/2 data port
///
/// - `val` - the byte that gets written to the data port
pub fn write_data_port(val: u8) -> PS2Result {
	wait_write()?;

	unsafe {
		outb(PORT_DATA, val);
	}

	Ok(())
}

/// Reads byte from PS/2 data port
pub fn read_data_port() -> PS2Data {
	wait_read()?;

	Ok(unsafe { inb(PORT_DATA) })
}
