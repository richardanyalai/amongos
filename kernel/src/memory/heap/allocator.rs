use crate::memory::paging::memory_map::get_memory_map;
use linked_list_allocator::LockedHeap;

#[global_allocator]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

pub fn init() {
	let (heap_start, heap_size) = get_memory_map()
		.map(|entry| (entry.base_addr, entry.length))
		.filter(|entry| entry.1 & 0xF == crate::bootboot::MMAP_FREE as u64)
		.max_by_key(|entry| entry.1)
		.unwrap();

	unsafe {
		ALLOCATOR
			.lock()
			.init(heap_start as *mut u8, heap_size as usize);
	}
}

pub fn info() -> (*mut u8, usize, usize, usize) {
	let allocator = ALLOCATOR.lock();
	(
		allocator.bottom(),
		allocator.size(),
		allocator.used(),
		allocator.free(),
	)
}

pub unsafe fn unlock_allocator() {
	ALLOCATOR.force_unlock();
}

#[cfg(not(test))]
#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
	panic!("Allocation error: {:?}", layout)
}
