//! Constants, structures and functions for page frames

use super::{PhysicalAddress, PAGE_SIZE};

/// Structure representing a page frame
///
/// * `number` - page table number
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Frame {
	pub number: usize,
}

impl Frame {
	/// Get the frame containing a specific address
	///
	/// * `address` - the address which has to be in the frame we are looking
	/// for
	pub fn containing_address(address: usize) -> Frame {
		Frame {
			number: address / PAGE_SIZE,
		}
	}

	/// Get the start address of a frame
	pub fn start_address(&self) -> PhysicalAddress {
		self.number * PAGE_SIZE
	}
}

/// Trait for frame allocators
pub trait FrameAllocator {
	fn allocate_frame(&mut self) -> Option<Frame>;
	fn deallocate_frame(&mut self, frame: Frame);
}
