//! Constants, structures and functions for page table entries

use super::frame::Frame;

/// Number of page table entries
pub const ENTRY_COUNT: usize = 512;

/// Enum constants representing page table entry flags
/// 
/// * `Present`        - the page is currently in memory
/// * `Writable`       - it's allowed to write to this page
/// * `UserAccessible` - if not set, only kernel mode code can access this page
/// * `WriteThrough`   - writes go directly to memory
/// * `NoCache`        - no cache is used for this page
/// * `Accessed`       - the CPU sets this bit when this page is used
/// * `Dirty`          - the CPU sets this bit when a write to this page occurs
/// * `HugePage`       - must be 0 in P1 and P4, creates a 1GiB page in P3,
/// creates a 2MiB page in P2
/// * `Global`         - page isn't flushed from caches on address space switch
/// (PGE bit of CR4 register must be set)
/// * `NoExecute`      - forbid executing code on this page (the NXE bit in the
/// EFER register must be set)
#[rustfmt::skip]
#[allow(dead_code, clippy::enum_clike_unportable_variant)]
#[derive(Copy, Clone)]
pub enum EntryFlags {
	Present        = 1 <<  0, //   1 -> 0b0000000000000001
	Writable       = 1 <<  1, //   2 -> 0b0000000000000010
	UserAccessible = 1 <<  2, //   4 -> 0b0000000000000100
	WriteThrough   = 1 <<  3, //   8 -> 0b0000000000001000
	NoCache        = 1 <<  4, //  16 -> 0b0000000000010000
	Accessed       = 1 <<  5, //  32 -> 0b0000000000100000
	Dirty          = 1 <<  6, //  64 -> 0b0000000001000000
	HugePage       = 1 <<  7, // 128 -> 0b0000000010000000
	Global         = 1 <<  8, // 256 -> 0b0000000100000000
	NoExecute      = 1 << 63,
}

use EntryFlags::*;

/// Structure representing a page table entry
///
/// * `value` - flags of the page table entry
pub struct Entry {
	pub value: u64,
}

impl Entry {
	/// Default constructor
	pub fn new() -> Entry {
		Entry { value: 0 }
	}

	/// Check whether the entry is unused
	pub fn is_unused(&self) -> bool {
		self.value == 0
	}

	/// Set the state of the entry to unused
	pub fn set_unused(&mut self) {
		self.value = 0;
	}

	/// Get the active flags of the entry
	#[rustfmt::skip]
	pub fn get_flags(&self) -> u64 {
		self.value & (
			Present        as u64 |
			Writable       as u64 |
			UserAccessible as u64 |
			WriteThrough   as u64 |
			NoCache        as u64 |
			Accessed       as u64 |
			Dirty          as u64 |
			HugePage       as u64 |
			Global         as u64 |
			NoExecute      as u64
		)
	}

	/// Check whether the entry has a specific flag enabled
	///
	/// * `flag` - the flag we are looking for among the flags of the entry
	pub fn contains_flag(&self, flag: EntryFlags) -> bool {
		self.value & flag as u64 == flag as u64
	}

	/// Enable a flag for the entry
	///
	/// * `flag` - the flag we want to enable the entry
	pub fn insert_flag(&mut self, flag: EntryFlags) {
		self.value |= flag as u64;
	}

	/// Disable a flag for the entry
	///
	/// * `flag` - the flag we want to disable the entry
	pub fn remove_flag(&mut self, flag: EntryFlags) {
		self.value &= !(flag as u64);
	}

	/// Toggle a flag for the entry
	///
	/// * `flag` - the flag we want to toggle the entry
	pub fn toggle_flag(&mut self, flag: EntryFlags) {
		self.value ^= flag as u64;
	}

	/// Get the frame that the entry points to
	///
	/// If the entry is present, we mask bits 12–51 and return the
	/// corresponding frame. If the entry is not present, it does not point to
	/// a valid frame so we return None.
	pub fn pointed_frame(&self) -> Option<Frame> {
		if self.contains_flag(Present) {
			Some(Frame::containing_address(
				self.value as usize & 0x000f_ffff_ffff_f000,
			))
		} else {
			None
		}
	}

	/// Set a flag for a specific frame
	///
	/// * `frame` - the frame we want to set a new flag on
	/// * `flag` - the flag we want to set
	pub fn set(&mut self, frame: Frame, flag: u64) {
		assert!(frame.start_address() & !0x000f_ffff_ffff_f000 == 0);
		self.value = (frame.start_address() as u64) | flag;
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn insert_flag() {
		let mut entry = Entry::new();

		entry.insert_flag(EntryFlags::Writable);
		entry.insert_flag(EntryFlags::Accessed);

		assert!(entry.contains_flag(EntryFlags::Writable));
		assert!(entry.contains_flag(EntryFlags::Accessed));
		assert!(!entry.contains_flag(EntryFlags::Dirty));
	}

	#[test]
	fn remove_flag() {
		let mut entry = Entry::new();

		entry.insert_flag(EntryFlags::Writable);

		assert!(entry.contains_flag(EntryFlags::Writable));

		entry.remove_flag(EntryFlags::Writable);

		assert!(!entry.contains_flag(EntryFlags::Writable));
	}

	#[test]
	fn toggle_flag() {
		let mut entry = Entry::new();

		entry.insert_flag(EntryFlags::Writable);

		assert!(entry.contains_flag(EntryFlags::Writable));

		entry.toggle_flag(EntryFlags::Writable);

		assert!(!entry.contains_flag(EntryFlags::Writable));
	}
}
