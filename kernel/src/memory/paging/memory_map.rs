use crate::bootboot::*;
use core::marker::PhantomData;

/// A memory area entry descriptor.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
#[rustfmt::skip]
pub struct MemoryArea {
	pub base_addr: u64,
	pub length   : u64,
	pub typ      : MemoryAreaType,
	    _reserved: u32,
}

impl MemoryArea {
	/// Creates a new instance with default values.
	#[rustfmt::skip]
	pub fn new() -> Self {
		MemoryArea {
			base_addr: 0,
			length   : 0,
			typ      : MemoryAreaType::Used,
			_reserved: 0
		}
	}

	/// The start address of the memory region.
	pub fn start_address(&self) -> u64 {
		self.base_addr
	}

	/// The end address of the memory region.
	pub fn end_address(&self) -> u64 {
		self.base_addr + self.length
	}

	/// The size, in bytes, of the memory region.
	pub fn size(&self) -> u64 {
		self.length
	}

	/// The type of the memory region.
	pub fn typ(&self) -> MemoryAreaType {
		self.typ
	}
}

/// Enum representing the type of a memory map area.
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
#[repr(u32)]
pub enum MemoryAreaType {
	Used = MMAP_USED,
	Free = MMAP_FREE,
	Acpi = MMAP_ACPI,
	Mmio = MMAP_MMIO,
}

#[rustfmt::skip]
pub fn get_memory_map() -> MemoryAreaIter<'static> {
	let bootboot_r   = unsafe { &(*(BOOTBOOT_INFO as *const Bootboot)) };
	let current_area = &bootboot_r.mmap as *const MMapEnt as u64;
	let last_area    = BOOTBOOT_INFO + bootboot_r.size as u64 - 16;
	let entry_size   = bootboot_r.mmap.size;

	MemoryAreaIter {
		current_area,
		last_area,
		entry_size,
		phantom: PhantomData {},
	}
}

/// An iterator over all memory areas
#[derive(Clone, Debug)]
#[rustfmt::skip]
pub struct MemoryAreaIter<'a> {
	current_area: u64,
	last_area   : u64,
	entry_size  : u64,
	phantom     : PhantomData<&'a MemoryArea>,
}

impl<'a> Iterator for MemoryAreaIter<'a> {
	type Item = &'a MemoryArea;
	fn next(&mut self) -> Option<&'a MemoryArea> {
		if self.current_area > self.last_area {
			None
		} else {
			let area = unsafe { &*(self.current_area as *const MemoryArea) };
			self.current_area += core::mem::size_of::<u128>() as u64;
			Some(area)
		}
	}
}
