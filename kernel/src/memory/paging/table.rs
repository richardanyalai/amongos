//! Constants, structures and functions for page tables

use core::{
	marker::PhantomData,
	ops::{Index, IndexMut},
};

use super::{
	entry::{Entry, EntryFlags::*, ENTRY_COUNT},
	frame::FrameAllocator,
};

pub const P4: *mut Table<Level4> = 0xffff_ffff_ffff_f000 as *mut _;

pub struct Table<L: TableLevel> {
	entries: [Entry; ENTRY_COUNT],
	level: PhantomData<L>,
}

impl<L> Table<L>
where
	L: TableLevel,
{
	pub fn zero(&mut self) {
		for entry in self.entries.iter_mut() {
			entry.set_unused();
		}
	}
}

impl<L> Table<L>
where
	L: HierarchicalLevel,
{
	fn next_table_address(&self, index: usize) -> Option<usize> {
		if self[index].contains_flag(Present)
			&& !self[index].contains_flag(HugePage)
		{
			let table_address = self as *const _ as usize;
			Some((table_address << 9) | (index << 12))
		} else {
			None
		}
	}

	pub fn next_table(&self, index: usize) -> Option<&Table<L::NextLevel>> {
		self.next_table_address(index)
			.map(|address| unsafe { &*(address as *const _) })
	}

	pub fn next_table_mut(
		&mut self,
		index: usize,
	) -> Option<&mut Table<L::NextLevel>> {
		self.next_table_address(index)
			.map(|address| unsafe { &mut *(address as *mut _) })
	}

	pub fn next_table_create<A>(
		&mut self,
		index: usize,
		allocator: &mut A,
	) -> &mut Table<L::NextLevel>
	where
		A: FrameAllocator,
	{
		if self.next_table(index).is_none() {
			assert!(
				!self.entries[index].contains_flag(HugePage),
				"Mapping code does not support huge pages"
			);

			let frame =
				allocator.allocate_frame().expect("No frames available");

			self.entries[index].set(frame, Present as u64 | Writable as u64);
			self.next_table_mut(index).unwrap().zero();
		}
		self.next_table_mut(index).unwrap()
	}
}

impl<L> Index<usize> for Table<L>
where
	L: TableLevel,
{
	type Output = Entry;

	fn index(&self, index: usize) -> &Entry {
		&self.entries[index]
	}
}

impl<L> IndexMut<usize> for Table<L>
where
	L: TableLevel,
{
	fn index_mut(&mut self, index: usize) -> &mut Entry {
		&mut self.entries[index]
	}
}

pub trait TableLevel {}

pub enum Level4 {}
pub enum Level3 {}
pub enum Level2 {}
pub enum Level1 {}

impl TableLevel for Level4 {}
impl TableLevel for Level3 {}
impl TableLevel for Level2 {}
impl TableLevel for Level1 {}

pub trait HierarchicalLevel: TableLevel {
	type NextLevel: TableLevel;
}

impl HierarchicalLevel for Level4 {
	type NextLevel = Level3;
}

impl HierarchicalLevel for Level3 {
	type NextLevel = Level2;
}

impl HierarchicalLevel for Level2 {
	type NextLevel = Level1;
}
