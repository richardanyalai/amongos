//! Constants, structures and functions for physical memory management

pub mod allocator;
pub mod entry;
pub mod frame;
pub mod memory_map;
pub mod page;
pub mod table;

use crate::{
	drivers::tty::tty_set_fgcolor,
	graphx::color::Colors::{LightGray, White},
	memory::paging::memory_map::get_memory_map,
	utils::logger::{kinf, kok, log},
};

pub type PhysicalAddress = usize;
pub type VirtualAddress = usize;

/// The size of one page
pub const PAGE_SIZE: usize = 4096;

pub fn init() {
	kinf!("Number of memory map entries: {}", get_memory_map().count());

	let avail_mem = get_memory_map()
		.map(|area| {
			tty_set_fgcolor!(LightGray);

			log!(
				"Mmap offset: {:12X}, size: {:indent$} bytes => {}",
				area.start_address(),
				area.size() & 0xFFFFFFFFFFFFFFF0,
				["used", "free", "ACPI", "ANVS", "MMIO"]
					[(area.size() & 0xF) as usize],
				indent = 12
			);

			tty_set_fgcolor!(White);

			area.size() & 0xFFFFFFFFFFFFFFF0
		})
		.reduce(|avail, curr| avail + curr)
		.unwrap();

	let memory = if avail_mem > 1024_u64.pow(3) {
		(avail_mem / 1024_u64.pow(3), "GigaBytes")
	} else if avail_mem > 1024_u64.pow(2) {
		(avail_mem / 1024_u64.pow(2), "MegaBytes")
	} else if avail_mem > 1024 {
		(avail_mem / 1024, "KiloBytes")
	} else {
		(avail_mem, "Bytes")
	};

	kinf!("Available memory: {} {}", memory.0, memory.1);
	kok!("Enabled paging");
}
