use super::{
	frame::{Frame, FrameAllocator},
	memory_map::{MemoryArea, MemoryAreaIter},
};

#[rustfmt::skip]
pub struct AreaFrameAllocator {
	next_free_frame: Frame,
	current_area   : Option<&'static MemoryArea>,
	areas          : MemoryAreaIter<'static>,
}

impl FrameAllocator for AreaFrameAllocator {
	fn allocate_frame(&mut self) -> Option<Frame> {
		if let Some(area) = self.current_area {
			// "Clone" the frame to return it if it's free. Frame doesn't
			// implement Clone, but we can construct an identical frame.
			let frame = Frame {
				number: self.next_free_frame.number,
			};

			// the last frame of the current area
			let current_area_last_frame = {
				let address = area.start_address() + area.size() - 1;
				Frame::containing_address(address as usize)
			};

			if frame > current_area_last_frame {
				// all frames of current area are used, switch to next area
				self.choose_next_area();
			} else {
				// frame is unused, increment `next_free_frame` and return it
				self.next_free_frame.number += 1;
				return Some(frame);
			}
			// `frame` was not valid, try it again with the updated
			// `next_free_frame`
			self.allocate_frame()
		} else {
			None // no free frames left
		}
	}

	fn deallocate_frame(&mut self, _frame: Frame) {
		unimplemented!()
	}
}

impl AreaFrameAllocator {
	pub fn new(memory_areas: MemoryAreaIter<'static>) -> AreaFrameAllocator {
		let mut allocator = AreaFrameAllocator {
			next_free_frame: Frame::containing_address(0),
			current_area: None,
			areas: memory_areas,
		};
		allocator.choose_next_area();

		allocator
	}

	fn choose_next_area(&mut self) {
		self.current_area = self
			.areas
			.clone()
			.filter(|area| {
				let address = area.start_address() + area.size() - 1;
				Frame::containing_address(address as usize)
					>= self.next_free_frame
			})
			.min_by_key(|area| area.start_address());

		if let Some(area) = self.current_area {
			let start_frame =
				Frame::containing_address(area.start_address() as usize);
			if self.next_free_frame < start_frame {
				self.next_free_frame = start_frame;
			}
		}
	}
}
