<div align="center" style="text-align:center">
	<img src="docs/images/logo-with-text.png" width="400px" />
</div>

## Introduction

KatyPerryOS is a minimalistic OS aiming the 64-bit Intel architecture,
written in Rust using the latest technologies. The booting process and the
setup of the framebuffer is handled by the
[bootboot](https://gitlab.com/bztsrc/bootboot) micro-kernel loader.

## Implemented features

- [x] Text console
- [x] Loading descriptor tables
- [x] Working interrupts
- [x] PIT timer
- [x] Serial port communication
- [x] Parallel port communication
- [x] PS/2 Keyboard driver
- [x] PS/2 Mouse driver
- [ ] Paging
- [x] Heap allocator
- [x] Internal clock (CMOS, RTC)
- [x] PCI devices
- [x] ACPI
- [ ] Multitasking
- [ ] ELF64 parser
- [ ] ATA driver
- [ ] Virtual file system
- [ ] Network driver
- [ ] Usermode
- [ ] Shell
- [ ] GUI, window management

## Photos

KatyPerryOS running in QEMU

![qemu](./docs/images/qemu.jpg)

KatyPerryOS running on real hardware (Lenovo Ideapad 5 15ARE05 with AMD Ryzen 5)

![realhw](./docs/images/real_hardware.jpg)

## Getting started

You will need _[Rust](https://www.rust-lang.org/learn/get-started), GNU Make_
and _QEMU_ installed on your machine.

After installing Rust you have to install the _xbuild_ and _src_ components
by running `cargo install cargo-xbuild cargo-src`

### Instructions for building and running the OS:

```sh
# building the bootable ISO
make

# running the OS in QEMU
make run

# cleaning the build directory
make clean

# writing bootable ISO to USB drive (run as root)
dd bs=4M conv=fsync oflag=direct status=progress if=build/aos.img of=/dev/sdx
```

## Documentation

For the full, comprehensive documentation please read the markdown documents in
the [docs](./docs) folder.

## Resources

[Rust](https://www.rust-lang.org/)  
[bootboot](https://gitlab.com/bztsrc/bootboot)  
[OSDev Wiki > Rust](https://wiki.osdev.org/Rust)  
[Writing an OS in Rust](https://os.phil-opp.com/)  
[This Month in Rust OSDev](https://rust-osdev.com/)

## Licensing

KatyPerryOS is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

KatyPerryOS is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
KatyPerryOS. If not, see <https://www.gnu.org/licenses/>.
