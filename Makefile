######################
###    Programs    ###
######################

SHELL		:= /bin/sh
QEMU		:= qemu-system-x86_64
MKBOOTIMG	:= ./mkbootimg/mkbootimg

######################
###  Directories   ###
######################

KERNEL_SRC	:= kernel/src
SYS_DIR		:= build/initrd/sys

######################
###     Files      ###
######################

KERNEL		:= $(SYS_DIR)/core
IMG			:= build/aos.iso

######################
###      Flags     ###
######################

.SHELLFLAGS	+= -e
QEMUFLAGS	:=	-serial stdio			\
				-rtc base=localtime		\
				-boot order=d,menu=off
				#-smp 6					\
				#-enable-kvm			\
				#-cpu host				\

######################
###    Targets     ###
######################

.PHONY: all clean run
.ONESHELL:

all: run

# Run with QEMU
run: $(IMG)
	@$(QEMU) $(QEMUFLAGS) -cdrom $(IMG)

# Bootable disk image
$(IMG): $(MKBOOTIMG) $(KERNEL) $(shell find config -type f -name "*")
	@$(MKBOOTIMG) config/mkbootimg.json $(IMG)

# Download the mkbootimg tool
$(MKBOOTIMG):
	@chmod +x ./mkbootimg/install.sh
	@./mkbootimg/install.sh

# Build the kernel
$(KERNEL): $(shell find $(KERNEL_SRC) -type f -name "*")
	@mkdir -p $(SYS_DIR) build/kernel
	@cd kernel
	@cargo fmt
	@cargo xbuild --target kernel-x86.json
	@cp target/kernel-x86/debug/kernel ../$(KERNEL)

# Run tests with cargo
test:
	@cd kernel
	@cargo test

# Run Clippy
clippy:
	@cd kernel
	@cargo xclippy --target kernel-x86.json

# Clean up the target and build folders
clean:
	@rm -rf build
	@cargo clean --manifest-path=kernel/Cargo.toml

# Run code formatter
fmt:
	@cd kernel
	@cargo fmt
# Aliases
t: test
c: clean
