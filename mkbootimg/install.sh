#/usr/bin/sh

cd mkbootimg

mkbootimg="https://gitlab.com/bztsrc/bootboot/-/blob/binaries/mkbootimg-Linux.zip"
[[ $OSTYPE == 'darwin'* ]] && mkbootimg="https://gitlab.com/bztsrc/bootboot/-/raw/binaries/mkbootimg-MacOSX.zip"

curl -o mkbootimg.zip -LO "$mkbootimg"
bsdtar -x -f mkbootimg.zip
rm -rf DESCRIPT.ION mkbootimg.zip
